<?php

class AAPP_Base_GET_Base_Api_Auth extends AAPP_Base_GET_Base_Api
{
    function get()
    {
        $Tpl = new Template($this->tpl, new ACMS_Corrector());
        $client = $this->getClient();
        $user = array();

        try {
            $json = $client->get('users/me');
            $user = $json->user;
        } catch ( Exception $e ) {}

        return $Tpl->render(array(
            'redirect_url' => $client->getAuthUrl(),
            'user' => $user,
        ));
    }
}