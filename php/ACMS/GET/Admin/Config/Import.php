<?php

class ACMS_GET_Admin_Config_Import extends ACMS_GET_Admin_Config
{
    function extendTemplate(&$vars, &$Tpl)
    {
        if ( $notice = $this->Post->getArray('notice') ) {
            foreach ( $notice as $message ) {
                $Tpl->add('notice:loop', $message);
            }
        }

        if ( $this->Post->get('import') === 'success' ) {
            $Tpl->add('success');
        }
    }
}
