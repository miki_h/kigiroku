<?php

define('ARCHIVES_BACKUP_DIR', SCRIPT_DIR.ARCHIVES_DIR.'backup/');
define('DB_FULL_BACKUP_DIR', SCRIPT_DIR.'private/backup/');
define('BLOG_EXPORT_DIR', SCRIPT_DIR.'private/import/');

class ACMS_GET_Admin_Backup_ArchiveZipList extends ACMS_GET
{
    function get()
    {
        $Tpl = new Template($this->tpl, new ACMS_Corrector());
        
        $zip_list = array();
        $sql_list = array();
        $import_list = array();
        
        $this->createList(ARCHIVES_BACKUP_DIR, $zip_list);
        $this->createList(DB_FULL_BACKUP_DIR, $sql_list);
        $this->createList(BLOG_EXPORT_DIR, $import_list);

        foreach($zip_list as $file){
            $Tpl->add('zip:loop', array(
                'zipfile' => $file,
            ));
        }
        
        foreach($sql_list as $file){
            $Tpl->add('sql:loop', array(
                'sqlfile' => $file,
            ));
        }

        foreach($import_list as $file){
            $Tpl->add('export:loop', array(
                'zip' => $file,
            ));
        }

        return $Tpl->get();
    }

    function createList($target, & $list)
    {
        if(is_dir($target)){
            if ($dir = opendir($target)) {
                while (($file = readdir($dir)) !== false) {
                    if ($file != "." && $file != ".." && substr($file,0,1) != '.') {
                        $list[] = $file;
                    }
                }
                closedir($dir);
            }
        }
    }
}
