<?php

class ACMS_GET_Admin_Messages extends ACMS_GET_Admin
{
    function get()
    {
        $Tpl = new Template($this->tpl, new ACMS_Corrector());

        if ( !$this->Post->isChildExists('messages')  ) {
            return '';
        }

        $messages = $this->Post->getChild('messages');
        $vars = $this->buildField($messages, $Tpl);
        $Tpl->add(null, $vars);

        return $Tpl->get();
    }
}
