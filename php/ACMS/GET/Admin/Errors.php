<?php

class ACMS_GET_Admin_Errors extends ACMS_GET_Admin
{
    function get()
    {
        $Tpl = new Template($this->tpl, new ACMS_Corrector());

        if ( !$this->Post->isChildExists('errors')  ) {
            return '';
        }

        $errors = $this->Post->getChild('errors');
        $vars = $this->buildField($errors, $Tpl);
        $Tpl->add(null, $vars);

        return $Tpl->get();
    }
}
