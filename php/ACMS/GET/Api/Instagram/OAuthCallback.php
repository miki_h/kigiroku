<?php

class ACMS_GET_Api_Instagram_OAuthCallback extends ACMS_GET_Api_Instagram
{
    function get()
    {
        $Tpl    = new Template($this->tpl, new ACMS_Corrector());

        // ログインチェック
        if ( !SUID && !sessionWithAdministration(BID) ) {
            $Tpl->add('unlogin');
            return $Tpl->get();
        }

        $code  = $this->Get->get('code');

        // request tokenの保持をチェック
        if ( !empty($code) ) {

            // access tokenの取得を試行
            $API    = ACMS_Services_Instagram::establish(BID);
            $acsUrl = $API->getAcsTokenUrl(array('code' => $code));

            $req = new HTTP_Request2($acsUrl, HTTP_Request2::METHOD_POST);
            $req->setAdapter('curl');
            $req->setConfig(array('timeout' => 10));
            $req->setHeader(array(
                'User-Agent' => 'ablogcms/' . VERSION,
                'Accept-Language' => HTTP_ACCEPT_LANGUAGE,
            ));
            $data = $req->getUrl()->getQueryVariables();
            if ( !empty($data) ) {
                foreach ( $data as $k => $v ) {
                    $req->addPostParameter($k, urldecode($v));
                }
            }

            try {
                $response = $req->send();
                $response = json_decode($response->getBody(), true);

                if ( !isset($response['access_token']) ) {
                    $Tpl->add('failed');
                } else {
                    $access = $response['access_token'];
                    $res = ACMS_Services_Instagram::insertAcsToken(BID, $access);
                    if ( empty($res) ) {
                        $Tpl->add('failed');
                    } else {
                        $Tpl->add('successed');
                    }
                }
            } catch ( HTTP_Request2_Exception $e ) {
                $Tpl->add('failed');
            }
        } else {
            $Tpl->add('failed');
        }

        return $Tpl->get();
    }
}
