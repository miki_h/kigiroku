<?php

namespace Acms\Services\Common;

use DB;
use SQL;
use Tpl;
use Storage;
use Image;
use Field;
use Field_Search;
use Field_Validation;
use Template;
use ACMS_Http;
use ACMS_Corrector;
use ACMS_POST_Image;
use ACMS_RAM;
use ACMS_Hook;

class Helper
{
    /**
     * @var \Field
     */
    protected $Post;

    /**
     * @var \Field
     */
    protected $Get;

    /**
     * @var \Field
     */
    protected $Q;

    /**
     * extract()後の削除フィールドを一時保存
     *
     * @var \Field
     */
    protected $deleteField;

    /**
     * Constructor
     */
    public function __construct()
    {
        $app = \App::getInstance();
        $this->Q =& $app->getQueryParameter();
        $this->Get =& $app->getGetParameter();
        $this->Post =& $app->getPostParameter();
    }

    /**
     * extract()後の削除フィールドを取得
     *
     * @return \Field
     */
    public function getDeleteField()
    {
        return $this->deleteField;
    }

    /**
     * メールテンプレートの解決
     *
     * @param string $path
     * @param Field $Field
     * @param string $charset
     *
     * @return string
     */
    public function getMailTxt ( $path, $field=null, $charset=null )
    {
        $tpl        = file_get_contents($path);
        $charset    = detectEncode($tpl);
        $tpl        = mb_convert_encoding($tpl, 'UTF-8', $charset);
        $tpl        = build($tpl, Field_Validation::singleton('post'), true);
        $Tpl        = new Template(setGlobalVars($tpl), new ACMS_Corrector());
        $vars       = Tpl::buildField($field, $Tpl);
        $Tpl->add(null, $vars);
        return buildIF($Tpl->get());
    }

    /**
     * メール設定の取得
     *
     * @param array $argConfig
     *
     * @return array
     */
    public function mailConfig ( $argConfig=array() )
    {
        $config = array();

        foreach ( array(
            'mail_smtp-host'        => 'smtp-host',
            'mail_smtp-port'        => 'smtp-port',
            'mail_smtp-user'        => 'smtp-user',
            'mail_smtp-pass'        => 'smtp-pass',
            'mail_smtp-auth_method' => 'smtp-auth_method',
            'mail_smtp-timeout'     => 'smtp-timeout',
            'mail_localhost'        => 'localhost',
            'mail_from'             => 'mail_from',
            'mail_sendmail_path'    => 'sendmail_path',
            'mail_sendmail_params'  => 'sendmail_params',
            'mail_rfc_2047_encoding'=> 'rfc_2047_encoding',
            'mail_rfc_2047_charset' => 'rfc_2047_charset',
            'mail_header_divider'   => 'crlf',
            
        ) as $cmsConfigKey => $mailConfigKey ) {
            $config[$mailConfigKey] = config($cmsConfigKey, '');
        }

        $config['additional_headers']   = 'X-Mailer: '
            .(LICENSE_OPTION_OEM ? LICENSE_OPTION_OEM : 'a-blog cms');

        $config['sendmail_path'] = ini_get('sendmail_path');

        if ( config('mail_additional_headers') ) {
            $config['additional_headers']   .= "\x0D\x0A".config('mail_additional_headers');
        }

        return $argConfig + $config;
    }

    /**
     * パスワードジェネレータ
     * 
     * @param int $len パスワードの長さ
     *
     * @return string
     */
    public function genPass($len)
    {
        $pass = '';
        for ( $i=0; $i<$len; $i++ ) {
            switch ( rand(0, 5) ) {
                case 0: // 0-9
                    if ( !$i ) {
                        $pass .= chr(rand(48, 57));
                        break;
                    }
                case 1: // A-Z
                case 2:
                    $pass .= chr(rand(65, 90));
                    break;
                default: // a-z
                    $pass .= chr(rand(97, 122));
            }
        }
        return $pass;
    }

    /**
     * エントリーのフルテキストを取得
     *
     * @param int $eid
     *
     * @return string
     */
    public function loadEntryFulltext($eid)
    {
        $DB = DB::singleton(dsn());
        $SQL = SQL::newSelect('column');
        $SQL->addWhereOpr('column_entry_id', $eid);
        $q = $SQL->get(dsn());

        $text = '';
        $meta = '';
        if ( $DB->query($q, 'fetch') and ($row = $DB->fetch($q)) ) { do {
            if ( $row['column_align'] === 'hidden' ) continue;
            $type = detectUnitTypeSpecifier($row['column_type']);
            if ( 'text' == $type ) {
                $_text  = $row['column_field_1'];
                if ( 'markdown' == $row['column_field_2'] ) {
                    $_text = \Michelf\MarkdownExtra::defaultTransform($_text);
                }
                $text   .= $_text.' ';
            } else if ( 'custom' == $type ) {
                $Custom = acmsUnserialize($row['column_field_6']);
                foreach ( $Custom->listFields() as $f ) {
                    $text   .= $Custom->get($f).' ';
                }
            } else {
                $meta   .= $row['column_field_1'].' ';
            }
        } while ( $row = $DB->fetch($q) ); }

        $meta .= $eid . ' ';
        $meta .= ACMS_RAM::entryTitle($eid) . ' ';
        $meta .= ACMS_RAM::entryCode($eid) . ' ';

        $SQL = SQL::newSelect('field');
        $SQL->addSelect('field_value');
        $SQL->addWhereOpr('field_search', 'on');
        $SQL->addWhereOpr('field_eid', $eid);
        $q = $SQL->get(dsn());

        if ( $DB->query($q, 'fetch') and ($row = $DB->fetch($q)) ) { do {
            $meta .= $row['field_value'].' ';
        } while ( $row = $DB->fetch($q) ); }

        $text = fulltextUnitData($text);
        return preg_replace('/\s+/u', ' ', strip_tags($text))
        ."\x0d\x0a\x0a\x0d".preg_replace('/\s+/u', ' ', strip_tags($meta))
            ;
    }

    /**
     * ユーザーのフルテキストを取得
     *
     * @param int $uid
     *
     * @return string
     */
    public function loadUserFulltext($uid)
    {
        $DB = DB::singleton(dsn());

        // ユーザーフィールド
        $user = array();

        // カスタムフィールド
        $meta = array();

        $user[] = ACMS_RAM::userName($uid);
        $user[] = ACMS_RAM::userCode($uid);
        $user[] = ACMS_RAM::userMail($uid);
        $user[] = ACMS_RAM::userMailMobile($uid);
        $user[] = ACMS_RAM::userUrl($uid);

        $SQL = SQL::newSelect('field');
        $SQL->addSelect('field_value');
        $SQL->addWhereOpr('field_search', 'on');
        $SQL->addWhereOpr('field_uid', $uid);
        $q = $SQL->get(dsn());

        if ( $DB->query($q, 'fetch') and ($row = $DB->fetch($q)) ) { do {
            $meta[] = $row['field_value'];
        } while ( $row = $DB->fetch($q) ); }

        $user = preg_replace('/\s+/u', ' ', strip_tags(implode(' ', $user)));
        $meta = preg_replace('/\s+/u', ' ', strip_tags(implode(' ', $meta)));
        return $user."\x0d\x0a\x0a\x0d".$meta;
    }

    /**
     * カテゴリのフルテキストを取得
     *
     * @param int $cid
     *
     * @return string
     */
    public function loadCategoryFulltext($cid)
    {
        $DB = DB::singleton(dsn());

        // カテゴリーフィールド
        $category = array();

        // カスタムフィールド
        $meta = array();

        $category[] = ACMS_RAM::categoryName($cid);
        $category[] = ACMS_RAM::categoryCode($cid);

        $SQL = SQL::newSelect('field');
        $SQL->addSelect('field_value');
        $SQL->addWhereOpr('field_search', 'on');
        $SQL->addWhereOpr('field_cid', $cid);
        $q = $SQL->get(dsn());

        if ( $DB->query($q, 'fetch') and ($row = $DB->fetch($q)) ) { do {
            $meta[] = $row['field_value'];
        } while ( $row = $DB->fetch($q) ); }

        $category = preg_replace('/\s+/u', ' ', strip_tags(implode(' ', $category)));
        $meta = preg_replace('/\s+/u', ' ', strip_tags(implode(' ', $meta)));
        return $category."\x0d\x0a\x0a\x0d".$meta;
    }

    /**
     * ブログのフルテキストを取得
     *
     * @param int $cid
     *
     * @return string
     */
    public function loadBlogFulltext($bid)
    {
        $DB = DB::singleton(dsn());

        // ブログフィールド
        $blog = array();

        // カスタムフィールド
        $meta = array();

        $blog[] = ACMS_RAM::blogName($bid);
        $blog[] = ACMS_RAM::blogCode($bid);
        $blog[] = ACMS_RAM::blogDomain($bid);

        $SQL = SQL::newSelect('field');
        $SQL->addSelect('field_value');
        $SQL->addWhereOpr('field_search', 'on');
        $SQL->addWhereOpr('field_bid', $bid);
        $q = $SQL->get(dsn());

        if ( $DB->query($q, 'fetch') and ($row = $DB->fetch($q)) ) { do {
            $meta[] = $row['field_value'];
        } while ( $row = $DB->fetch($q) ); }

        $blog = preg_replace('/\s+/u', ' ', strip_tags(implode(' ', $blog)));
        $meta = preg_replace('/\s+/u', ' ', strip_tags(implode(' ', $meta)));
        return $blog."\x0d\x0a\x0a\x0d".$meta;
    }

    /**
     * プラグインフルテキストを取得
     *
     * @param int $cuid
     *
     * @return string
     */
    public function loadPluginFulltext($cuid)
    {
        $DB = DB::singleton(dsn());

        // ユーザーフィールド
        $user = array();

        // カスタムフィールド
        $meta = array();

        $SQL = SQL::newSelect('crm_customer');
        $SQL->addWhereOpr('customer_id', $cuid);
        if ( $row = $DB->query($SQL->get(dsn()), 'row') ) {
            foreach ( $row as $key => $val ) {
                $user[] = $val;
            }
        }
        $SQL = SQL::newSelect('crm_field');
        $SQL->addSelect('field_value');
        $SQL->addWhereOpr('field_customer_id', $cuid);
        $q = $SQL->get(dsn());

        if ( $DB->query($q, 'fetch') and ($row = $DB->fetch($q)) ) { do {
            $meta[] = $row['field_value'];
        } while ( $row = $DB->fetch($q) ); }

        $user = preg_replace('/\s+/u', ' ', strip_tags(implode(' ', $user)));
        $meta = preg_replace('/\s+/u', ' ', strip_tags(implode(' ', $meta)));
        
        return $user."\x0d\x0a\x0a\x0d".$meta;
    }

    /**
     * フルテキストの保存
     *
     * @param string $type フルテキストのタイプ
     * @param int $id
     * @param string $fulltext
     *
     * @return void
     */
    public function saveFulltext($type, $id, $fulltext=null)
    {
        $DB = DB::singleton(dsn());
        $SQL = SQL::newDelete('fulltext');
        $SQL->addWhereOpr('fulltext_'.$type, $id);
        if ( $type !== 'bid' ) {
            $SQL->addWhereOpr('fulltext_blog_id', BID);
        }
        $DB->query($SQL->get(dsn()), 'exec');

        if ( !empty($fulltext) ) {
            $SQL    = SQL::newInsert('fulltext');
            $SQL->addInsert('fulltext_value', $fulltext);
            if ( config('ngram') ) {
                $SQL->addInsert('fulltext_ngram', 
                    preg_replace('/(　|\s)+/u', ' ', join(' ', ngram(strip_tags($fulltext), config('ngram'))))
                );
            }
            $SQL->addInsert('fulltext_'.$type, $id);
            $SQL->addInsert('fulltext_blog_id', BID);
            $DB->query($SQL->get(dsn()), 'exec');
        }
    }

    /**
     * プラグインのフルテキスト保存
     *
     * @param string $type フルテキストのタイプ
     * @param int $id
     * @param string $fulltext
     *
     * @return void
     */
    public function savePluginFulltext($type, $id, $fulltext=null)
    {
        $DB     = DB::singleton(dsn());
        $SQL    = SQL::newDelete('fulltext_plugin');
        $SQL->addWhereOpr('fulltext_key', $type);
        $SQL->addWhereOpr('fulltext_id', $id);
        $DB->query($SQL->get(dsn()), 'exec');

        if ( !empty($fulltext) ) {
            $SQL    = SQL::newInsert('fulltext_plugin');
            $SQL->addInsert('fulltext_value', $fulltext);
            $SQL->addInsert('fulltext_key', $type);
            $SQL->addInsert('fulltext_id', $id);
            $SQL->addInsert('fulltext_blog_id', BID);
            $DB->query($SQL->get(dsn()), 'exec');
        }
    }

    /**
     * カスタムフィールドの保存
     *
     * @param string $type
     * @param int $id
     * @param Field $Field
     * @param Field $deleteField
     * @param int $rvid
     * @param string $moveFieldArchive
     *
     * @return bool
     */
    public function saveField($type, $id, $Field=null, $deleteField=null, $rvid=null, $moveFieldArchive='')
    {
        if ( empty($id) ) return false;

        $DB                 = DB::singleton(dsn());
        $ARCHIVES_DIR_TO    = REVISON_ARCHIVES_DIR;
        $tableName          = 'field';
        $revision           = false;

        if ( 1
            && enableRevision(false)
            && $rvid
            && $type == 'eid' 
        ) {
            if ( $moveFieldArchive === 'ARCHIVES_DIR' ) {
                $ARCHIVES_DIR_TO = ARCHIVES_DIR;
                $FieldOld = loadEntryField($id);
                foreach ( $FieldOld->listFields() as $fd ) {
                    if ( 1
                        and !strpos($fd, '@path')
                        and !strpos($fd, '@tinyPath')
                        and !strpos($fd, '@largePath')
                        and !strpos($fd, '@squarePath')
                    ) {
                        continue;
                    }
                    foreach ( $FieldOld->getArray($fd, true) as $i => $val ) {
                        $path = $val;
                        if ( !is_file($ARCHIVES_DIR_TO.$path) ) continue;
                        @unlink($ARCHIVES_DIR_TO.$path);
                    }
                }
            } else {
                $tableName = 'field_rev';
            }
            $revision = true;
        }

        if ( 1
            && $revision
            && intval($rvid) === 1
            && empty($moveFieldArchive)
        ) {
            $FieldOld = loadEntryField($id, 1);
            foreach ( $FieldOld->listFields() as $fd ) {
                if ( 1
                    and !strpos($fd, '@path')
                    and !strpos($fd, '@tinyPath')
                    and !strpos($fd, '@largePath')
                    and !strpos($fd, '@squarePath')
                ) {
                    continue;
                }
                foreach ( $FieldOld->getArray($fd, true) as $i => $val ) {
                    $path = $val;
                    if ( !is_file(REVISON_ARCHIVES_DIR.$path) ) continue;
                    @unlink(REVISON_ARCHIVES_DIR.$path);
                }
            }
        }

        $SQL    = SQL::newDelete($tableName);
        $SQL->addWhereOpr('field_'.$type, $id);
        if ( $tableName  === 'field_rev' ) {
            $SQL->addWhereOpr('field_rev_id', $rvid);
        }
        if ( $Field && $Field->get('updateField') === 'on' ) {
            $fkey   = array();
            $Field->delete('updateField');
            foreach ( $Field->listFields() as $fd ) {
                $fkey[] = $fd;
            }
            if ( $this->deleteField ) {
                foreach ( $this->deleteField->listFields() as $fd ) {
                    $fkey[] = $fd;
                }
            }
            $SQL->addWhereIn('field_key', $fkey);
        }
        $DB->query($SQL->get(dsn()), 'exec');

        $temp   = '';
        if ( !empty($moveFieldArchive) ) {
            $temp   = 'TEMP/';
        }

        if ( !empty($Field) ) {
            foreach ( $Field->listFields() as $fd ) {
                // copy revision
                if ( $revision ) {
                    if ( strpos($fd, '@path') ) {
                        $list   = $Field->getArray($fd, true);
                        $base   = substr($fd, 0, (-1 * strlen('@path')));
                        $set    = false;
                        foreach ( $list as $i => $val ) {
                            $path   = $val;
                            if ( is_file(ARCHIVES_DIR.$temp.$path) ) {
                                $info       = pathinfo($path);
                                $dirname    = empty($info['dirname']) ? '' : $info['dirname'].'/';
                                Storage::makeDirectory($ARCHIVES_DIR_TO.$dirname);
                                $ext        = empty($info['extension']) ? '' : '.'.$info['extension'];
                                $newPath    = $dirname.uniqueString().$ext;

                                $path       = ARCHIVES_DIR.$temp.$path;
                                $largePath  = otherSizeImagePath($path, 'large');
                                $tinyPath   = otherSizeImagePath($path, 'tiny');
                                $squarePath = otherSizeImagePath($path, 'square');

                                $newLargePath   = otherSizeImagePath($newPath, 'large');
                                $newTinyPath    = otherSizeImagePath($newPath, 'tiny');
                                $newSquarePath  = otherSizeImagePath($newPath, 'square');

                                @copy($path, $ARCHIVES_DIR_TO.$newPath);
                                @copy($largePath, $ARCHIVES_DIR_TO.$newLargePath);
                                @copy($tinyPath, $ARCHIVES_DIR_TO.$newTinyPath);
                                @copy($squarePath, $ARCHIVES_DIR_TO.$newSquarePath);

                                if ( !$set ) {
                                    $Field->set($fd, $newPath);
                                    if ( is_readable($largePath) ) {
                                        $Field->set($base.'@largePath', $newLargePath);
                                    }
                                    if ( is_readable($tinyPath) ) {
                                        $Field->set($base.'@tinyPath', $newTinyPath);
                                    }
                                    if ( is_readable($squarePath) ) {
                                        $Field->set($base.'@squarePath', $newSquarePath);
                                    }
                                    $set = true;
                                } else {
                                    $Field->add($fd, $newPath);
                                    if ( is_readable($largePath) ) {
                                        $Field->add($base.'@largePath', $newLargePath);
                                    }
                                    if ( is_readable($tinyPath) ) {
                                        $Field->add($base.'@tinyPath', $newTinyPath);
                                    }
                                    if ( is_readable($squarePath) ) {
                                        $Field->add($base.'@squarePath', $newSquarePath);
                                    }
                                    
                                }
                            }
                        }
                    }
                }

                foreach ( $Field->getArray($fd, true) as $i => $val ) {
                    $SQL    = SQL::newInsert($tableName);
                    $SQL->addInsert('field_key', $fd);
                    $SQL->addInsert('field_value', $val);
                    $SQL->addInsert('field_sort', $i + 1);
                    $SQL->addInsert('field_search', $Field->getMeta($fd, 'search') ? 'on' : 'off');
                    $SQL->addInsert('field_'.$type, $id);
                    $SQL->addInsert('field_blog_id', BID);
                    if ( $tableName  === 'field_rev' ) {
                        $SQL->addInsert('field_rev_id', $rvid);
                    }
                    $DB->query($SQL->get(dsn()), 'exec');
                }
            }
        }
        return true;
    }

    /**
     * URIオブジェクトの取得
     *
     * @param \Field $Post
     *
     * @return \Field
     */
    public function getUriObject($Post)
    {
        $Uri = new Field();

        //-----
        // arg
        if ( !$aryFd = $Post->getArray('arg') ) {
            $aryFd  = array_diff($Post->listFields(), $Post->getArray('field'), $Post->getArray('query'));
        }
        foreach ( $aryFd as $fd ) {
            //---------
            // field
            if ( 'field' == $fd and $aryField = $Post->getArray('field') ) {
                $Field  = new Field_Search();
                foreach ( $aryField as $j => $fd ) {
                    $Field->set($fd);
                    $Field->setConnector($fd);
                    $Field->setOperator($fd);
                    $aryValue       = $Post->getArray($fd);
                    $aryConnector   = $Post->getArray($fd.'@connector');
                    $aryOperator    = $Post->getArray($fd.'@operator');
                    $Field->addSeparator($fd, $Post->get($fd.'@separator', 'and'));
                    
                    if ( !!($cnt = max(count($aryValue), count($aryConnector), count($aryOperator))) ) {
                        $defaultConnector   = 'and';
                        $defaultOperator    = 'eq';
                        if ( empty($aryConnector) and empty($aryOperator) /*and 2 <= count($aryValue)*/ ) {
                            $defaultConnector   = 'or';
                        }
                        if ( !empty($aryConnector) ) {
                            $defaultConnector   = $aryConnector[0];
                        }
                        if ( !empty($aryOperator) ) {
                            $defaultOperator    = $aryOperator[0];
                        }
                        for ( $i=0; $i<$cnt; $i++ ) {
                            $Field->add($fd, isset($aryValue[$i]) ? $aryValue[$i] : '');
                            $Field->addConnector($fd, isset($aryConnector[$i]) ? $aryConnector[$i] : $defaultConnector);
                            $Field->addOperator($fd, isset($aryOperator[$i]) ? $aryOperator[$i] : $defaultOperator);
                        }
                    }
                }
                $Uri->addChild('field', $Field);

            //-------
            // query
            } else if ( 'query' == $fd and $aryQuery = $Post->getArray('query') ) {
                $Query  = new Field();
                foreach ( $aryQuery as $fd ) {
                    $Query->set($fd, $Post->getArray($fd));
                }
                $Uri->addChild('query', $Query);

            //-------
            // value
            } else {
                $Uri->set($fd, $Post->getArray($fd));
            }
        }
        return $Uri;
    }

    /**
     * POSTデータからデータの抜き出し
     *
     * @param string $scp
     * @param \Field $V
     * @param \Field $deleteField
     * @param bool $moveArchive
     *
     * @return \Field
     */
    public function extract($scp='field', $V=null, $deleteField=null, $moveArchive=false)
    {
        $Field = new Field_Validation();
        $this->deleteField = $deleteField;

        $ARCHIVES_DIR = ARCHIVES_DIR;
        if ( $moveArchive ) { 
            $ARCHIVES_DIR = ARCHIVES_DIR.'TEMP/';
        }

        if ( !$this->deleteField ) $this->deleteField = new Field();

        if ( $takeover = $this->Post->get($scp.':takeover') ) {
            $Field->overload(acmsUnserialize($takeover));
            $this->Post->delete($scp.':takeover');
        }

        $Field->overload($this->Post->dig($scp));
        $this->Post->addChild($scp, $Field);

        // 許可ファイル拡張子をまとめておく
        $allow_file_extensions = array_merge(
            configArray('file_extension_document'),
            configArray('file_extension_archive'),
            configArray('file_extension_movie'),
            configArray('file_extension_audio')
        );

        //-------
        // child
        foreach ( $Field->listFields() as $fd ) {
            if ( !$this->Post->isExists($fd.':field') ) continue;
            $this->Post->set($fd, $Field->getArray($fd));
            $Field->delete($fd);
            $Field->addChild($fd, $this->extract($fd));
        }

        foreach ( $this->Post->listFields() as $metaFd ) {
            //-----------
            // converter
            if ( 1
                and preg_match('@^(.+)(?:\:c|\:converter)$@', $metaFd, $match)
                and $Field->isExists($match[1])
            ) {
                $fd = $match[1];
                $aryVal = array();
                foreach ( $Field->getArray($fd) as $val ) {
                    $aryVal[] = mb_convert_kana($val, $this->Post->get($metaFd), 'UTF-8');
                }
                $Field->setField($fd, $aryVal);
                $this->Post->delete($metaFd);
                continue;
            }
            //-----------
            // extension
            if ( 1
                and preg_match('@^(.+):extension$@', $metaFd, $match)
                and $Field->isExists($match[1])
            ) {
                $fd         = $match[1];
                $type       = $this->Post->get($fd.':extension');
                $dataUrl    = false;
                $this->Post->delete($fd.':extension');

                if ( $type === 'image' || $type === 'file' ) {
                    try {
                        $file = ACMS_Http::file($fd);

                        if ( $extensions = $this->Post->getArray($fd.'@extension') ) {
                            $extension_entity = pathinfo($file->getName(), PATHINFO_EXTENSION);
                            $extension_matched = false;
                            foreach ( $extensions as $extension ) {
                                if ( $extension === $extension_entity ) {
                                    $extension_matched = true;
                                }
                            }
                            if ( !$extension_matched ) {
                                throw new \RuntimeException('EXTENSION_IS_DIFFERENT');
                            }
                        }

                        $size = $file->getFileSize();
                        if ( isset($Field->_aryMethod[$fd]) ) {
                            $arg = $Field->_aryMethod[$fd];
                            if ( isset($arg['filesize']) ) {
                                $maxsize = intval($arg['filesize']);
                                if ( $size > ($maxsize * 1024) ) {
                                    throw new \RuntimeException(UPLOAD_ERR_FORM_SIZE);
                                }
                            }
                        }
                    } catch ( \Exception $e ) {
                        if ( $e->getMessage() == 'EXTENSION_IS_DIFFERENT' ) {
                            $Field->setMethod($fd, 'extension', false);
                            continue;
                        }
                        if ( $e->getMessage() == UPLOAD_ERR_INI_SIZE || $e->getMessage() == UPLOAD_ERR_FORM_SIZE ) {
                            $Field->setMethod($fd, 'filesize', false);
                            $Field->set($fd, 'maxfilesize');
                            continue;
                        }
                    }
                }

                //-------
                // image
                if ( 'image' == $type ) {
                    // data url
                    if ( isset($_POST[$fd]) ) {
                        ACMS_POST_Image::base64DataToImage($_POST[$fd], $fd);
                        $Field->delete($fd);
                        $dataUrl = true;
                    }

                    if ( empty($_FILES[$fd]) ) { 
                        foreach ( array(
                            'path', 'x', 'y', 'alt', 'fileSize',
                            'largePath', 'largeX', 'largeY', 'largeAlt', 'largeFileSize',
                            'tinyPath', 'tinyX', 'tinyY', 'tinyAlt', 'tinyFileSize',
                            'squarePath', 'squareX', 'squareY', 'squareAlt', 'squareFileSize',
                            'secret'
                        ) as $key ) {
                            $key    = $fd.'@'.$key;
                            $this->deleteField->set($key, array());
                            $Field->deleteField($fd.'@'.$key);
                        }
                        continue; 
                    }
                    
                    $aryC   = array();
                    if ( !is_array($_FILES[$fd]['tmp_name']) ) {
                        $aryC[] = array(
                            '_tmp_name' => $_FILES[$fd]['tmp_name'],
                            '_name'     => $_FILES[$fd]['name'],
                        );
                    } else {
                        foreach ( $_FILES[$fd]['tmp_name'] as $i => $tmp_name ) {
                            $aryC[] = array(
                                '_tmp_name' => $tmp_name,
                                '_name'     => $_FILES[$fd]['name'][$i],
                            );
                        }
                    }

                    foreach ( array(
                        'str'   => array('old', 'edit', 'alt', 'filename', 'extension', 'secret'),
                        'int'   => array(
                            'width', 'height', 'size',
                            'tinyWidth', 'tinyHeight', 'tinySize',
                            'largeWidth', 'largeHeight', 'largeSize',
                            'squareWidth', 'squareHeight', 'squareSize',
                        ),
                    ) as $_type => $keys ) {
                        foreach ( $keys as $key ) {
                            foreach ( $aryC as $i => $c ) {
                                $_field = $fd.'@'.$key;
                                $value  = $this->Post->isExists($_field, $i) ?
                                    $this->Post->get($_field, '', $i) : '';
                                $c[$key]    = ('int' == $_type) ? intval($value) : strval($value);
                                $aryC[$i]   = $c;
                            }
                            $this->Post->delete($fd.'@'.$key);
                        }
                    }

                    $aryData    = array();
                    foreach ( $aryC as $c ) {
                        $aryData[]  = array();
                    }
                    $cnt    = count($aryData);
                    for ( $i=0; $i<$cnt; $i++ ) {
                        $c          = $aryC[$i];
                        $data       =& $aryData[$i];
                        $pattern    = '@'.REVISON_ARCHIVES_DIR.'@';
                        $c['old']   = preg_replace($pattern, '', $c['old'], 1);

                        //-------------
                        // rawfilename
                        if ( preg_match('/^@(.*)$/', $c['filename'], $match) ) {
                            $c['filename']  = ('rawfilename' == $match[1]) ? $c['_name'] : '';
                        }

                        //------------------------------------
                        // security check ( nullバイトチェック )
                        if ( $c['old']      !== ltrim($c['old']) ) { continue; }
                        if ( $c['filename'] !== ltrim($c['filename']) ) { continue; }

                        //-------------------------------------------------------------
                        // パスの半正規化 ( directory traversal対策・バイナリセーフ関数を使用 )
                        // この時点で //+ や ^/ は 混入する可能性はあるが無害とみなす
                        $c['old']      = preg_replace('/\.+\/+/', '', $c['old']);
                        $c['filename'] = preg_replace('/\.+\/+/', '', $c['filename']);

                        //---------------------------------------------
                        // 例外的無視ファイル
                        // pathの終端（ファイル名）が特定の場合にリジェクトする
                        if ( !!preg_match('/\.htaccess$/', $c['filename']) ) { continue; }
                        //---------------------
                        // セキュリティチェック
                        // リクエストされた削除ファイル名が怪しい場合に削除と上書きをスキップ
                        // このチェックに引っかかった場合にもフィールドの情報は保持する(continueしない)
                        // 削除キーがDBに保存されていなかった場合などファイルが消せなくなるため
                        // 救済措置としてDEBUG_MODEがONの時にはこのチェックを行わない
                        // 投稿者以上の権限を持っている場合にもチェックを行わない
                        // 暗号化は「フィールド名@パス」をmd5したもの
                        // 暗号化文字列の照合にDBは使えない
                        // 一回目にフォームを送信するときはDB上にデータがない
                        // アップロードが完了したにもかかわらず
                        // 他のエラーチェックで引っかかった時は
                        // DB上にデータは保存されないため比較できない
                        $secretCheck = ( 1
                            and !DEBUG_MODE
                            and !$moveArchive
                            and !sessionWithContribution()
                            and !empty($c['old'])
                            and ( 0
                                or 'delete' == $c['edit']
                                or !empty($c['_tmp_name'])
                            )
                        ) ? ($c['secret'] == md5($fd.'@'.$c['old'])) : true;

                        //----------------------------
                        // delete ( 指定削除 continue )
                        if ( 1
                            and 'delete' == $c['edit']
                            and !empty($c['old'])
                            and $secretCheck
                            and empty($tmpMedia)
                            and isExistsRuleModuleConfig()
                        ) {
                            Image::deleteImageAllSize($ARCHIVES_DIR.normalSizeImagePath($c['old']));
                            continue;
                        }

                        //--------
                        // upload
                        if ( !empty($c['_tmp_name']) and $secretCheck ) {

                            $tmp_name   = $c['_tmp_name'];
                            if ( !$dataUrl && !is_uploaded_file($tmp_name) ) { continue; }
                            // getimagesizeが画像ファイルであるかの判定を兼用している
                            // @todo security:
                            // "GIF89a <SCRIPT>alert('xss');< /SCRIPT>のようなテキストファイルはgetimagesizeを通過する
                            // IE6, 7あたりはContent-Typeのほかにファイルの中身も評価してしまう
                            // 偽装テキストを読み込んだときに、HTML with JavaScriptとして実行されてしまう可能性がある
                            // 参考: http://www.tokumaru.org/d/20071210.html
                            if ( !($xy = @getimagesize($tmp_name)) ) { continue; }

                            //---------------------------
                            // delete ( 古いファイルの削除 )
                            if ( 1
                                and !empty($c['old'])
                                and empty($tmpMedia)
                                and isExistsRuleModuleConfig()
                            ) {
                                Image::deleteImageAllSize($ARCHIVES_DIR.normalSizeImagePath($c['old']));
                            }

                            //------------------------------
                            // dirname, basename, extension
                            if ( !empty($c['filename']) ) {

                                if ( !preg_match('@((?:[^/]*/)*)((?:[^.]*\.)*)(.*)$@', sprintf('%03d', BID).'/'.$c['filename'], $match) ) { 
                                    trigger_error('unknown', E_USER_ERROR); 
                                }

                                $extension  = !empty($match[3]) ? $match[3]
                                                                : Image::detectImageExtenstion($xy['mime']);
                                $dirname    = $match[1];
                                $basename   = !empty($match[2]) ? $match[2].$extension
                                                                : uniqueString().'.'.$extension;

                            } else {

                                $extension  = !empty($c['extension']) ? $c['extension']
                                                                      : Image::detectImageExtenstion($xy['mime']);
                                $dirname    = Storage::archivesDir();
                                $basename   = uniqueString().'.'.$extension;
                            }

                            //-------
                            // angle
                            $angle  = 0;
                            if ( 'rotate' == substr($c['edit'], 0, 6) ) {
                                $angle  = intval(substr($c['edit'], 6));
                            }

                            //--------
                            // normal
                            $normal     = $dirname.$basename;
                            $normalPath = $ARCHIVES_DIR.$normal;

                            if ( !file_exists($normalPath) ) { Image::copyImage($tmp_name, $normalPath, $c['width'], $c['height'], $c['size'], $angle); }

                            if ( $xy = @getimagesize($normalPath) ) {
                                $data[$fd.'@path']  = $normal;
                                $data[$fd.'@x']     = $xy[0];
                                $data[$fd.'@y']     = $xy[1];
                                $data[$fd.'@alt']   = $c['alt'];
                                $data[$fd.'@fileSize'] = filesize($normalPath);

                                $tmpMedia[] = array(
                                    'path'  => $normalPath,
                                );
                            }

                            //-------
                            // large
                            if ( !empty($c['largeWidth']) or !empty($c['largeHeight']) or !empty($c['largeSize']) ) {
                                $large     = $dirname.'large-'.$basename;
                                $largePath = $ARCHIVES_DIR.$large;
                                if ( !file_exists($largePath) ) { Image::copyImage($tmp_name, $largePath, $c['largeWidth'], $c['largeHeight'], $c['largeSize'], $angle); }
                                if ( $xy = @getimagesize($largePath) ) {
                                    $data[$fd.'@largePath'] = $large;
                                    $data[$fd.'@largeX']    = $xy[0];
                                    $data[$fd.'@largeY']    = $xy[1];
                                    $data[$fd.'@largeAlt']  = $c['alt'];
                                    $data[$fd.'@largeFileSize']  = filesize($largePath);

                                    $tmpMedia[] = array(
                                        'path'  => $normalPath,
                                    );
                                }
                            }

                            //------
                            // tiny
                            if ( !empty($c['tinyWidth']) or !empty($c['tinyHeight']) or !empty($c['tinySize']) ) {
                                $tiny     = $dirname.'tiny-'.$basename;
                                $tinyPath = $ARCHIVES_DIR.$tiny;
                                if ( !file_exists($tinyPath) ) { Image::copyImage($tmp_name, $tinyPath, $c['tinyWidth'], $c['tinyHeight'], $c['tinySize'], $angle); }
                                if ( $xy = @getimagesize($tinyPath) ) {
                                    $data[$fd.'@tinyPath']  = $tiny;
                                    $data[$fd.'@tinyX']     = $xy[0];
                                    $data[$fd.'@tinyY']     = $xy[1];
                                    $data[$fd.'@tinyAlt']   = $c['alt'];
                                    $data[$fd.'@tinyFileSize']  = filesize($tinyPath);

                                    $tmpMedia[] = array(
                                        'path'  => $normalPath,
                                    );
                                }
                            }
                            
                            //---------
                            // square
                            if ( !empty($c['squareWidth']) or !empty($c['squareHeight']) or !empty($c['squareSize']) ) {
                                $square   = $dirname.'square-'.$basename;
                                $squarePath = $ARCHIVES_DIR.$square;
                                $squareSize = 0;
                                if ( !empty($c['squareWidth']) ) {
                                    $squareSize = $c['squareWidth'];
                                } else if ( !empty($c['squareHeight']) ) {
                                    $squareSize = $c['squareHeight'];
                                } else if ( !empty($c['squareSize']) ) {
                                    $squareSize = $c['squareSize'];
                                }
                                
                                if ( !file_exists($squarePath) ) { Image::copyImage($tmp_name, $squarePath, $squareSize, $squareSize, $squareSize, $angle); }
                                if ( $xy = @getimagesize($squarePath) ) {
                                    $data[$fd.'@squarePath']  = $square;
                                    $data[$fd.'@squareX']     = $xy[0];
                                    $data[$fd.'@squareY']     = $xy[1];
                                    $data[$fd.'@squareAlt']   = $c['alt'];
                                    $data[$fd.'@squareFileSize']  = filesize($squarePath);

                                    $tmpMedia[] = array(
                                        'path'  => $normalPath,
                                    );
                                }
                            }

                            //--------
                            // secret
                            // 正しくファイルがアップロードされた場合のみ新しくキーを発行する
                            $data[$fd.'@secret'] = md5($fd.'@'.$normal);

                            continue;
                        }

                        //-----
                        // old
                        // 非編集アップデートの時
                        if ( !empty($c['old']) ) {

                            //--------
                            // normal
                            $normal = $c['old'];
                            $normalPath = $ARCHIVES_DIR.$normal;
                            if ( $xy = @getimagesize($normalPath) ) {
                                $data[$fd.'@path']  = $normal;
                                $data[$fd.'@x']     = $xy[0];
                                $data[$fd.'@y']     = $xy[1];
                                $data[$fd.'@alt']   = $c['alt'];
                                $data[$fd.'@fileSize'] = filesize($normalPath);

                                if ( !preg_match('@((?:[^/]*/)*)((?:[^.]*\.)*)(.*)$@', $normal, $match) ) { 
                                    trigger_error('unknown', E_USER_ERROR); 
                                }
                                $extension  = $match[3];
                                $dirname    = $match[1];
                                $basename   = $match[2].$extension;

                                //-------
                                // large
                                $large     = $dirname.'large-'.$basename;
                                $largePath = $ARCHIVES_DIR.$large;
                                if ( $xy = @getimagesize($largePath) ) {
                                    $data[$fd.'@largePath'] = $large;
                                    $data[$fd.'@largeX']    = $xy[0];
                                    $data[$fd.'@largeY']    = $xy[1];
                                    $data[$fd.'@largeAlt']  = $c['alt'];
                                    $data[$fd.'@largeFileSize']  = filesize($largePath);
                                }

                                //------
                                // tiny
                                $tiny     = $dirname.'tiny-'.$basename;
                                $tinyPath = $ARCHIVES_DIR.$tiny;
                                if ( $xy = @getimagesize($tinyPath) ) {
                                    $data[$fd.'@tinyPath']  = $tiny;
                                    $data[$fd.'@tinyX']     = $xy[0];
                                    $data[$fd.'@tinyY']     = $xy[1];
                                    $data[$fd.'@tinyAlt']   = $c['alt'];
                                    $data[$fd.'@tinyFileSize']  = filesize($tinyPath);
                                }
                                
                                //------
                                // square
                                $square   = $dirname.'square-'.$basename;
                                $squarePath = $ARCHIVES_DIR.$square;
                                if ( $xy = @getimagesize($squarePath) ) {
                                    $data[$fd.'@squarePath']  = $square;
                                    $data[$fd.'@squareX']     = $xy[0];
                                    $data[$fd.'@squareY']     = $xy[1];
                                    $data[$fd.'@squareAlt']   = $c['alt'];
                                    $data[$fd.'@squareFileSize']  = filesize($squarePath);
                                }


                                //--------
                                // secret
                                // これはエラー時にフォームを再表示しなければならない場合に必要
                                $data[$fd.'@secret']  = $c['secret'];
                            }
                        }
                    }

                    //------------
                    // save field
                    $cnt        = count($aryData);
                    foreach ( array(
                        'path', 'x', 'y', 'alt', 'fileSize',
                        'largePath', 'largeX', 'largeY', 'largeAlt', 'largeFileSize',
                        'tinyPath', 'tinyX', 'tinyY', 'tinyAlt', 'tinyFileSize',
                        'squarePath', 'squareX', 'squareY', 'squareAlt', 'squareFileSize',
                        'secret'
                    ) as $key ) {
                        $key    = $fd.'@'.$key;
                        $value  = array();
                        for ( $i=0; $cnt>$i; $i++ ) {
                            $value[] = !empty($aryData[$i][$key]) ? $aryData[$i][$key] : '';
                        }
                        $Field->set($key, $value);

                        //------------
                        // validation
                        foreach ( $this->Post->listFields() as $_fd ) {
                            if ( preg_match('/^'.$key.':(?:v#|validator#)(.+)$/', $_fd, $match) ) {
                                $method = $match[1];
                                $Field->setMethod($key, $method, $this->Post->get($_fd));
                                $this->Post->delete($_fd);
                            }
                        }
                    }

                //------
                // file
                } else if ( 'file' == $type ) {
                    
                    if ( empty($_FILES[$fd]) ) {
                        $this->deleteField->setField($fd.'@path', array());
                        $this->deleteField->setField($fd.'@baseName', array());
                        $this->deleteField->setField($fd.'@fileSize', array());
                        $this->deleteField->setField($fd.'@secret', array());

                        $Field->deleteField($fd.'@path');
                        $Field->deleteField($fd.'@baseName');
                        $Field->deleteField($fd.'@fileSize');
                        $Field->deleteField($fd.'@secret');
                        continue; 
                    }

                    $aryC   = array();
                    if ( !is_array($_FILES[$fd]['tmp_name']) ) {
                        $aryC[] = array(
                            '_tmp_name' => $_FILES[$fd]['tmp_name'],
                            '_name'     => $_FILES[$fd]['name'],
                        );
                    } else {
                        foreach ( $_FILES[$fd]['tmp_name'] as $i => $tmp_name ) {
                            $aryC[] = array(
                                '_tmp_name' => $tmp_name,
                                '_name'     => $_FILES[$fd]['name'][$i],
                            );
                        }
                    }
                    
                    //--------------------------
                    // field copy to local vars
                    foreach ( array('old', 'edit', 'extension', 'filename', 'secret', 'fileSize') as $key ) {
                        foreach ( $aryC as $i => $c ) {
                            $_field = $fd.'@'.$key;
                            if ( $key === 'extension' ) {
                                $c[$key] = $this->Post->isExists($_field, $i) ?
                                    $this->Post->getArray($_field, '', $i) : '';
                            } else {
                                $c[$key] = $this->Post->isExists($_field, $i) ?
                                    $this->Post->get($_field, '', $i) : '';
                            }
                            $aryC[$i] = $c;
                        }
                        $this->Post->delete($fd.'@'.$key);
                    }

                    // 参照用の配列を作成して，ファイル数の分だけインデックスを初期化
                    $aryPath    = array();
                    $aryName    = array();
                    $aryOriginalName = array();
                    $arySize    = array();
                    $arySecret  = array();
                    foreach ( $aryC as $c ) {
                        $aryPath[] = $aryName[] = $aryOriginalName[] = $arySize[] = $arySecret[] = '';
                    }
                    
                    $cnt    = count($aryPath);

                    for ( $i=0; $i<$cnt; $i++ ) {
                        $c      = $aryC[$i];
                        // 各配列のインデックス位置を，ローカル変数に参照させる
                        $_path  =& $aryPath[$i];
                        $_name  =& $aryName[$i];
                        $_orginal_name =& $aryOriginalName[$i];
                        $_size  =& $arySize[$i];
                        $_secret=& $arySecret[$i];


                        $pattern    = '@'.REVISON_ARCHIVES_DIR.'@';
                        $c['old']   = preg_replace($pattern, '', $c['old'], 1);

                        //-------------
                        // rawfilename
                        if ( preg_match('/^@(.*)$/', $c['filename'], $match) ) {
                            $c['filename']  = ('rawfilename' == $match[1]) ? $c['_name'] : '';
                        }

                        //------------------------------------
                        // security check ( nullバイトチェック )
                        if ( $c['old']      !== ltrim($c['old']) ) { continue; }
                        if ( $c['filename'] !== ltrim($c['filename']) ) { continue; }

                        //-------------------------------------------------------------
                        // パスの半正規化 ( directory traversal対策・バイナリセーフ関数を使用 )
                        // この時点で //+ や ^/ は 混入する可能性はあるが無害とみなす
                        $c['old']      = preg_replace('/\.+\/+/', '', $c['old']);
                        $c['filename'] = preg_replace('/\.+\/+/', '', $c['filename']);

                        //---------------------------------------------
                        // 例外的無視ファイル
                        // pathの終端（ファイル名）が特定の場合にリジェクトする
                        if ( !!preg_match('/\.htaccess$/', $c['filename']) ) { continue; }

                        //---------------------
                        // シークレットチェック
                        $secretCheck = ( 1
                            and !DEBUG_MODE
                            and !sessionWithContribution()
                            and !empty($c['old'])
                            and ( 0
                                or 'delete' == $c['edit']
                                or !empty($c['_tmp_name'])
                            )
                        ) ? ($c['secret'] == md5($fd.'@'.$c['old'])) : true;

                        //----------------------------
                        // delete ( 指定削除 continue )
                        if ( 'delete' == $c['edit'] and !empty($c['old']) and $secretCheck and empty($tmpMedia) ) {
                            @unlink($ARCHIVES_DIR.$c['old']);
                            if ( HOOK_ENABLE ) {
                                $Hook = ACMS_Hook::singleton();
                                $Hook->call('mediaDelete', $ARCHIVES_DIR.$c['old']);
                            }
                            continue;
                        }

                        //--------
                        // upload
                        if ( !empty($c['_tmp_name']) and $secretCheck ) {

                            $tmp_name   = $c['_tmp_name'];
                            if ( !is_uploaded_file($tmp_name) ) { continue; }
                            // 拡張子がなければリジェクト
                            if ( !preg_match('@\.([^.]+)$@', $c['_name'], $match) ) { continue; }

                            // テキストファイル（=PHPなどのスクリプトファイル）判定
                            // ファイルの先頭1000行を取得
                            // 文字コードが判別不能な文字列をバイナリとみなす
                            if ( 'on' == config('file_prohibit_textfile') ) {
                                $fp = fopen($c['_tmp_name'], 'rb');
                                $readedLine = 0;
                                $sampleLine = 1000;
                                $sample = '';

                                while ( ($line = fgets($fp, 4096)) !== false ) {
                                    if ( $readedLine++ > $sampleLine ) { break; }
                                    $sample .= $line;
                                }

                                fclose($fp);

                                // @todo security:
                                // mb_detect_encodingを利用しているが、これはUTF-16を判定できないため、バイナリファイルと見なしてしまう
                                // 冒頭をUTF-16、以後をUTF-8にすることで不正なテキストファイルをarchivesにアップロードできる可能性がある
                                // ただし、htaccessをいじられたりしない限りは基本的に問題にならない（通常はPHP等として実行できない）
                                if ( false !== detectEncode($sample) ) { continue; }
                            }

                            //------------------------------
                            // dirname, basename, extension
                            // アップロードされた実ファイルの拡張子が実質的に利用される
                            // extensionオプションや、filenameオプションの制限は、
                            // 意図する拡張子のファイルがアップロードされているかのチェックのみに使われる

                            // 実ファイルの拡張子
                            $extension  = $match[1]; 

                            if ( !empty($c['filename']) ) {
                                if ( !preg_match('@((?:[^/]*/)*)((?:[^.]*\.)*)(.*)$@', sprintf('%03d', BID).'/'.$c['filename'], $match) ) { 
                                    trigger_error('unknown', E_USER_ERROR); 
                                }

                                // @filenameオプションの拡張子
                                $c['filename_extension']  = $match[3];

                                // @filenameオプションの指定内に拡張子がないと，ファイル名とファイル名の拡張子が同一になる | @todo issue: 先行する正規表現を改善する
                                // ディレクトリのみでファイル名は無指定の場合は、拡張子が空になる
                                //   =>  ファイル名拡張子でチェックする意図がないものとして、filename_extensionをunsetし、以降の拡張子チェックから除外する
                                if ( $c['filename'] === $c['filename_extension'] || empty($c['filename_extension']) ) {
                                    unset($c['filename_extension']);
                                }

                                $dirname    = $match[1];
                                $basename   = !empty($match[2]) ? $match[2].$extension      // basenameは実ファイルの拡張子とする
                                                                : uniqueString().'.'.$extension;
                            } else {
                                $dirname    = Storage::archivesDir();
                                $basename   = uniqueString().'.'.$extension;
                            }

                            if ( 1
                                // "実ファイルの拡張子" が "アップロード許可拡張子コンフィグ" に含まれていること
                                and in_array($extension, $allow_file_extensions)

                                // 拡張子指定オプションが空でなければ...
                                and ( empty($c['extension']) or
                                    (
                                        // "拡張子指定オプション" が "アップロード許可拡張子コンフィグ" に含まれていること
                                        array_in_array($c['extension'], $allow_file_extensions)
                                        
                                        // さらに，"拡張子指定オプション" と "実ファイルの拡張子" が一致すること
                                        and in_array($extension, $c['extension']))
                                    )

                                // ファイル名オプションの拡張子が未定義でなければ...
                                and ( !isset($c['filename_extension']) or
                                    (
                                        // "ファイル名オプションの拡張子" が "アップロード許可拡張子コンフィグ" に含まれていること
                                        in_array($c['filename_extension'], $allow_file_extensions))

                                        // さらに，"ファイル名オプションの拡張子" と "実ファイルの拡張子" が一致すること
                                        and $c['filename_extension'] === $extension
                                    )

                                // 保存先ディレクトリの再帰的作成
                                and Storage::makeDirectory($ARCHIVES_DIR.$dirname)
                            ) {
                                //---------------------------
                                // delete ( 古いファイルの削除 )
                                if ( !empty($c['old']) && empty($tmpMedia) ) {
                                    @unlink($ARCHIVES_DIR.$c['old']);
                                    if ( HOOK_ENABLE ) {
                                        $Hook = ACMS_Hook::singleton();
                                        $Hook->call('mediaDelete', $ARCHIVES_DIR.$c['old']);
                                    }
                                }

                                //------
                                // copy
                                $path     = $dirname.$basename;
                                $realpath = $ARCHIVES_DIR.$path;
                                if ( !file_exists($realpath) ) {
                                    copy($tmp_name, $realpath);
                                    chmod($realpath, intval(config('permission_file')));

                                    $tmpMedia[] = array(
                                        'path'  => $realpath,
                                    );

                                    if ( HOOK_ENABLE ) {
                                        $Hook = ACMS_Hook::singleton();
                                        $Hook->call('mediaCreate', $realpath);
                                    }
                                }

                                //-----
                                // set
                                $_path  = $path;
                                $_name  = basename($realpath);
                                $_orginal_name = $c['_name'];
                                $_size  = filesize($realpath);
                                $_secret= md5($fd.'@'.$path);
                                continue;

                            } else {
                                $Field->setMethod($fd, 'inValidFile', false);
                            }
                        }

                        //-----
                        // old
                        if ( !empty($c['old']) ) {
                            $_path  = $c['old'];
                            $_secret= $c['secret'];
                            $_size  = $c['fileSize'];
                            continue;
                        }
                    }

                    //-----------
                    // set field
                    $Field->setField($fd.'@path',     $aryPath);
                    $Field->setField($fd.'@baseName', $aryName);
                    $Field->setField($fd.'@fileSize', $arySize);
                    $Field->setField($fd.'@secret', $arySecret);
                    $Field->setField($fd.'@originalName', $aryOriginalName);

                    //------------
                    // validation
                    $key    = $fd.'@path';
                    foreach ( $this->Post->listFields() as $_fd ) {
                        if ( preg_match('/^'.$key.':(?:v#|validator#)(.+)$/', $_fd, $match) ) {
                            $method = $match[1];
                            $Field->setMethod($key, $method, $this->Post->get($_fd));
                            $this->Post->delete($_fd);
                        }
                    }

                }
                // file
                //------

                continue;
            }
        }

        //--------
        // search
        foreach ( $Field->listFields() as $fd ) {
            // topic-fix_field_search: Field::getがnullを返さなくなっていたので，無指定時の戻りを擬似定数に変更して対処
            $s  = $this->Post->get($fd.':search', '__NOT_SPECIFIED__');
            if ( $s === '__NOT_SPECIFIED__' ) {
                if ( is_int(strpos($fd, '@')) ) {
                    $s  = '0';
                } else {
                    $s  = '1';
                }
            }
            $Field->setMeta($fd, 'search', !empty($s));
            $this->Post->deleteField($fd.':search');
        }

        $Field->validate($V);

        return $Field;
    }
}