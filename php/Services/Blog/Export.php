<?php

namespace Acms\Services\Blog;

use SQL;
use DB;
use Symfony\Component\Yaml\Yaml;

class Export
{
    protected $yaml;
    protected $tables;

    public function __construct()
    {
        $this->yaml = array();
        $this->setTables(array(
            'category', 'column', 'config', 'comment',
            'dashboard', 'entry', 'field', 'form', 'fulltext',
            'module', 'rule', 'tag', 'schedule', 'layout_grid',
        ));
    }

    /**
     * export blog data
     *
     * @param int $bid
     *
     * @return string
     */
    public function run($bid)
    {
        foreach ( $this->tables as $table ) {
            $SQL = SQL::newSelect($table);
            $SQL->addWhereOpr($table.'_blog_id', $bid);
            $records = DB::query($SQL->get(dsn()), 'all');

            $this->fix($records, $table);
            $this->setYaml($records, $table);
        }

        return $this->getYaml();
    }

    /**
     * set export tables
     *
     * @param array $tables
     *
     * @return void
     *
     * @throws \RuntimeException
     */
    public function setTables($tables=array())
    {
        if ( !is_array($tables) ) {
            throw new \RuntimeException('Not specified tables.');
        }
        $this->tables = $tables;
    }

    /**
     * get data as array
     *
     * @return array
     */
    public function getArray()
    {
        return $this->yaml;
    }

    /**
     * get data as yaml
     *
     * @return string
     */
    public function getYaml()
    {
        return Yaml::dump($this->yaml, 2, 4);
    }

    /**
     * set data as yaml
     *
     * @param array $records
     * @param string $table
     *
     * @return void
     */
    private function setYaml($records=array(), $table)
    {
        $this->yaml[$table] = $records;
    }

    /**
     * fix data
     *
     * @param &array $records
     * @param string $table
     *
     * @return void
     */
    private function fix(& $records, $table)
    {
        if ( $table === 'column' ) {
            $this->fixMarkdown($records);
            $this->fixHalfSpace($records);
        }
        if ( $table === 'schedule' ) {
            $this->fixSchedule($records);
        }
        if ( $table === 'fulltext' ) {
            $this->fixFulltext($records);
        }
    }

    /**
     * escape single byte spaces of row's header
     *
     * @param $records
     *
     * @return void
     */
    private function fixHalfSpace(& $records)
    {
        $cnt = count($records);

        for ( $i=0; $i < $cnt; $i++ ) {
            if ( 'text' !== $records[$i]['column_type'] ) {
                continue;
            }
            $txt    = $records[$i]['column_field_1'];
            $txt    = preg_replace('/^([ ]+)/m', '|s|$1', $txt);
            /*
             * carrige returns \r and \r\n
             * Paragraph Separator (U+2028)
             * Line Separator (U+2029)
             * Next Line (NEL) (U+0085)
             */
            $records[$i]['column_field_1'] = preg_replace('/(\xe2\x80[\xa8-\xa9]|\xc2\x85|\r\n|\r)/', "\n", $txt);
        }
    }

    /**
     * escape sharp(#) headlines of Markdown
     *
     * @param $records
     *
     * @return void
     */
    private function fixMarkdown(& $records)
    {
        $cnt    = count($records);

        for ( $i=0; $i < $cnt; $i++ ) {
            if ( 1
                and preg_match('@^(#*)(.*)$@', $records[$i]['column_field_1'])
                and 'text' == $records[$i]['column_type']
            ) {
                $records[$i]['column_field_1'] .= "\x0D\x0A";
            }
        }
    }

    /**
     * ignore user fulltext
     *
     * @param $records
     */
    private function fixFulltext(& $records)
    {
        $cnt    = count($records);
        for ( $i=0; $i < $cnt; $i++ ) {
            if ( !empty($records[$i]['fulltext_uid']) ) {
                unset($records[$i]);
            }
        }
        $records = array_values($records);
    }

    /**
     * ignore schedule data without base set
     *
     * @param $records
     */
    private function fixSchedule(& $records)
    {
        $cnt    = count($records);

        for ( $i=0; $i < $cnt; $i++ ) {
            if ( 1
                and $records[$i]['schedule_year']   == 0000
                and $records[$i]['schedule_month']  == 00
            ) {
                // no touch
            } else {
                unset($records[$i]);
            }
        }
        $records = array_values($records);
    }
}