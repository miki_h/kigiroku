<?php

namespace Acms\Services\Blog;

use SQL;
use DB;
use Symfony\Component\Yaml\Yaml;

class Import
{
    /**
     * @var array
     */
    protected $yaml;

    /**
     * @var int
     */
    protected $bid;

    /**
     * @var int
     */
    protected $uid;

    /**
     * @var array
     */
    protected $ids;

    /**
     * @var array
     */
    protected $errors;

    /**
     * Import constructor
     */
    public function __construct()
    {
        $this->uid = SUID;
    }

    /**
     * import blog data
     *
     * @param int $bid
     * @param string $yaml
     *
     * @return array
     */
    public function run($bid, $yaml)
    {
        $this->bid = $bid;
        $this->yaml = Yaml::parse($yaml);
        $this->ids = array();
        $this->errors = array();

        $this->dropData();
        $this->registerNewIDs();

        $tables = array(
            'category', 'entry', 'tag',
            'fulltext', 'module', 'layout_grid',
            'rule', 'config', 'column', 'dashboard', 'field'
        );
        foreach ( $tables as $table ) {
            $this->insertData($table);
        }

        return $this->errors;
    }

    /**
     * @param string $table
     *
     * @return void
     */
    private function insertData($table)
    {
        if ( !$this->existsYaml($table) ) {
            return;
        }
        foreach ( $this->yaml[$table] as $record ) {
            $SQL = SQL::newInsert($table);
            foreach ( $record as $field => $value ) {
                $value = $this->fix($table, $field, $value);
                if ( is_callable(array($this, $table . 'Fix')) ) {
                    $value = call_user_func_array(array($this, $table . 'Fix'), array($field, $value));
                }
                if ( $value !== false ) {
                    $SQL->addInsert($field, $value);
                }
            }
            try {
                DB::query($SQL->get(dsn()), 'exec');
            } catch ( \Exception $e ) {
                $this->errors[] = $e->getMessage();
            }
        }
    }

    /**
     * @param $table
     * @param $field
     * @param $value
     *
     * @return int
     */
    private function fix($table, $field, $value)
    {
        $key = substr($field, strlen($table . '_'));
        if ( $key === 'id' ) {
            $value = $this->getNewID($table, $value);
        } else if ( $key === 'category_id' ) {
            $value = $this->getNewID('category', $value);
        } else if ( $key === 'user_id' ) {
            $value = $this->getNewID('user', $value);
        } else if ( $key === 'entry_id' ) {
            $value = $this->getNewID('entry', $value);
        } else if ( $key === 'rule_id' ) {
            $value = $this->getNewID('rule', $value);
        } else if ( $key === 'module_id' ) {
            $value = $this->getNewID('module', $value);
        } else if ( $key === 'blog_id' ) {
            $value = $this->bid;
        }
        if ( in_array($key, array(
                'id', 'category_id', 'user_id', 'entry_id',
                'rule_id', 'module_id', 'blog_id'
            )) && empty($value)
        ) {
            $value = null;
        }
        return $value;
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    private function entryFix($field, $value)
    {
        if ( $field === 'entry_current_rev_id' ) {
            $value = 0;
        } else if ( $field === 'entry_last_update_user_id' ) {
            $value = $this->uid;
        }
        return $value;
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    private function categoryFix($field, $value)
    {
        if ( $field === 'category_parent' ) {
            $value = $this->getNewID('category', $value);
        }
        return $value;
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    private function fulltextFix($field, $value)
    {
        if ( $field === 'fulltext_eid' ) {
            $value = $this->getNewID('entry', $value);
        } else if ( $field === 'fulltext_cid' ) {
            $value = $this->getNewID('category', $value);
        } else if ( $field === 'fulltext_uid' ) {
            $value = $this->getNewID('user', $value);
        }
        if ( empty($value) ) {
            if ( $field === 'fulltext_ngram' ) {
                $value = '';
            } else {
                $value = null;
            }
        }
        if ( $field === 'fulltext_bid' && !empty($value) ) {
            return false;
        }
        return $value;
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    private function moduleFix($field, $value)
    {
        if ( $field === 'module_eid' ) {
            $value = $this->getNewID('entry', $value);
        } else if ( $field === 'module_cid' ) {
            $value = $this->getNewID('category', $value);
        } else if ( $field === 'module_uid' ) {
            $value = $this->getNewID('user', $value);
        } else if ( $field === 'module_bid' ) {
            $value = $this->bid;
        }
        return $value;
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    private function layout_gridFix($field, $value)
    {
        if ( $field === 'layout_grid_mid' ) {
            $value = $this->getNewID('module', $value);
        }
        return $value;
    }


    /**
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    private function ruleFix($field, $value)
    {
        if ( $field === 'rule_eid' ) {
            $value = $this->getNewID('entry', $value);
        } else if ( $field === 'rule_cid' ) {
            $value = $this->getNewID('category', $value);
        } else if ( $field === 'rule_uid' ) {
            $value = $this->getNewID('user', $value);
        } else if ( $field === 'rule_aid' ) {
            $value = $this->getNewID('alias', $value);
        } else if ( $field === 'rule_bid' ) {
            $value = $this->bid;
        }
        return $value;
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    private function fieldFix($field, $value)
    {
        if ( $field === 'field_eid' ) {
            $value = $this->getNewID('entry', $value);
        } else if ( $field === 'field_cid' ) {
            $value = $this->getNewID('category', $value);
        } else if ( $field === 'field_uid' ) {
            $value = $this->getNewID('user', $value);
        } else if ( $field === 'field_mid' ) {
            $value = $this->getNewID('module', $value);
        } else if ( $field === 'field_bid' ) {
            $value = $this->bid;
        }
        return $value;
    }

    /**
     * @param string $table
     * @param int $id
     *
     * @return int
     */
    private function getNewID($table, $id)
    {
        if ( !isset($this->ids[$table][$id])  ) {
            return $id;
        }
        return $this->ids[$table][$id];
    }

    /**
     * @return void
     */
    private function registerNewIDs()
    {
        $tables = array(
            'category', 'column', 'alias',
            'entry', 'fulltext', 'media',
            'module', 'rule',
        );

        foreach ( $tables as $table ) {
            $this->registerNewID($table);
        }
    }

    /**
     * @param string $table
     *
     * @return void
     */
    private function registerNewID($table)
    {
        if ( !$this->existsYaml($table) ) {
            return;
        }
        foreach ( $this->yaml[$table] as $record ) {
            if ( !isset($record[$table . '_id']) ) {
                continue;
            }
            $id = $record[$table . '_id'];
            if ( isset($this->ids[$table][$id]) ) {
                continue;
            }
            $this->ids[$table][$id] = DB::query(SQL::nextval($table . '_id', dsn()), 'seq');
        }
    }

    /**
     * check yaml data
     *
     * @param $table
     *
     * @return bool
     */
    private function existsYaml($table)
    {
        if ( !isset($this->yaml[$table]) ) {
            return false;
        }
        $data = $this->yaml[$table];
        if ( !is_array($data) ) {
            return false;
        }
        return true;
    }

    /**
     * drop blog data
     *
     * @return void
     */
    private function dropData()
    {
        $tables = array(
            'category', 'entry', 'column', 'tag',
            'fulltext', 'field',
            'approval', 'cache_reserve', 'column_rev', 'entry_rev',
            'field_rev', 'tag_rev',
            'dashboard', 'module', 'layout_grid', 'rule', 'config',
        );

        foreach ( $tables as $table ) {
            $this->clearTable($table);
        }
    }

    /**
     * clear table in database
     *
     * @param string $table
     *
     * @return void
     */
    private function clearTable($table)
    {
        $SQL = SQL::newDelete($table);
        if ( preg_match('/^(.*)\_rev$/', $table, $match) ) {
            $SQL->addWhereOpr($match[1].'_blog_id', $this->bid);
        } else {
            $SQL->addWhereOpr($table.'_blog_id', $this->bid);
        }
        DB::query($SQL->get(dsn()), 'exec');
    }


}