<?php

namespace Acms\Services\Storage\Contracts;

use Config;

abstract class Base
{
    /**
     * @var int
     */
    protected $fileMod;

    /**
     * @var int $directoryMod
     */
    protected $directoryMod;

    /**
     * Base constructor
     */
    public function __construct()
    {

    }

    /**
     * @param int $mod
     *
     * @return void
     */
    public function setFileMod($mod)
    {
        $this->fileMod = $mod;
    }

    /**
     * @param int $mod
     *
     * @return void
     */
    public function setDirectoryMod($mod)
    {
        $this->directoryMod = $mod;
    }

    /**
     * @param string $path
     *
     * @return void
     */
    public function changeMod($path)
    {
        if ( is_dir($path)  ) {
            chmod($path, intval($this->directoryMod));
        } else {
            chmod($path, intval($this->fileMod));
        }
    }

}