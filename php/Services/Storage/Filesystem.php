<?php

namespace Acms\Services\Storage;

use Acms\Services\Storage\Contracts\Filesystem as FilesystemInterface;
use Acms\Services\Storage\Contracts\Base;
use Symfony\Component\Finder\Finder;
use PclZip;

class Filesystem extends Base implements FilesystemInterface
{
    /**
     * @param string $path
     *
     * @return bool
     */
    public function exists($path)
    {
        return file_exists($path);
    }

    /**
     * @param $path
     *
     * @return mixed
     *
     * @throws \RuntimeException
     */
    public function get($path)
    {
        if ( is_file($path) ) {
            return file_get_contents($path);
        }
        throw new \RuntimeException("File does not exist at path {$path}");
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public function remove($path)
    {
        if ( is_dir($path) ) {
            return $this->removeDirectory($path);
        }

        if ( file_exists($path) ) {
            return @unlink($path);
        }

        return false;
    }

    /**
     * @param string $path
     * @param string $contents
     *
     * @return bool
     */
    public function put($path, $contents)
    {
        $byte = @file_put_contents($path, $contents);
        if ( $byte ) {
            $this->changeMod($path);
            return $byte;
        }
        throw new \RuntimeException('failed to put contents in ' . $path);
    }

    /**
     * @param string $from
     * @param string $to
     *
     * @return bool
     */
    public function copy($from, $to)
    {
        $res = copy($from, $to);
        $this->changeMod($to);

        return $res;
    }

    /**
     * @param string $from
     * @param string $to
     *
     * @return bool
     */
    public function move($from, $to)
    {
        return rename($from, $to);
    }

    /**
     * @param string $dir
     *
     * @return bool
     */
    public function removeDirectory($dir)
    {
        if ( !is_dir($dir) ) {
            return false;
        }

        $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ( $files as $file ) {
            if ( is_dir($file) ) {
                $this->removeDirectory($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);

        return true;
    }

    /**
     * @param string $from
     * @param string $to
     *
     * @return bool
     */
    public function copyDirectory($from, $to)
    {
        if ( !is_dir($from) ) {
            return false;
        }

        $this->makeDirectory($to);
        $dir = opendir($from);
        while ( false !== ($file = readdir($dir)) ) {
            if ( $file !== '.' && $file !== '..' ) {
                if ( is_dir($from . '/' . $file) ) {
                    $this->copyDirectory($from . '/' . $file, $to . '/' . $file);
                } else {
                    copy($from . '/' . $file, $to . '/' . $file);
                    $this->changeMod($to . '/' . $file);
                }
            }
        }
        closedir($dir);
        return true;
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public function makeDirectory($path)
    {
        $dir = '';
        foreach ( preg_split("@(/)@", $path, -1, PREG_SPLIT_DELIM_CAPTURE) as $i => $token ) {
            $dir .= $token;
            if ( empty($dir) ) continue;
            if ( '/' === $token ) continue;
            if ( !is_dir($dir) ) {
                mkdir($dir);
                $this->changeMod($dir);
            }
        }
        return $dir;
    }

    /**
     * @param $path
     *
     * @return int Unix time stamp
     */
    public function lastModified($path)
    {
        if ( $this->exists($path) ) {
            return filemtime($path);
        }

        return 0;
    }

    /**
     * @return string
     */
    public function archivesDir()
    {
        return sprintf('%03d', BID) . '/' . date('Ym') . '/';
    }

    /**
     * @param string $source
     * @param string $destination
     * @param string $root
     * @param array $exclude
     *
     * @return void
     */
    public function compress($source, $destination, $root = '', $exclude = array())
    {
        $zip = new PclZip($destination);

        $finder = new Finder();
        $finder = $finder->in($source);
        foreach ( $exclude as $item ) {
            $finder->exclude($item);
        }
        $iterator = $finder->files();

        foreach ( $iterator as $file ) {
            $path = $source . $file->getRelativePathname();
            $zip->add($path,
                PCLZIP_OPT_REMOVE_PATH, $source,
                PCLZIP_OPT_ADD_PATH, $root
            );
        }
        $zip->extract();
    }

    /**
     * @param string $source
     * @param string $destination
     *
     * @return void
     *
     * @throws \RuntimeException
     */
    public function unzip($source, $destination)
    {
        $zip = new PclZip($source);
        if ( $zip->extract(PCLZIP_OPT_PATH, $destination) === 0 ) {
            throw new \RuntimeException($zip->errorInfo(true));
        }
    }
}