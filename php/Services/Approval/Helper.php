<?php

namespace Acms\Services\Approval;

use DB;
use SQL;
use ACMS_RAM;
use Field;

class Helper
{
    /**
     * 通知数をカウント
     *
     * @return int
     */
    public function notificationCount()
    {
        $SQL = $this->buildSql();
        if ( !($all = DB::query($SQL->get(dsn()), 'all')) ) {
            return 0;
        }
        $count  = 0;
        foreach ( $all as $row ) {
            $exceptUsers = explode(',', $row['notification_except_user_ids']);
            if ( in_array(strval(SUID), $exceptUsers) ) {
                continue;
            }
            if ( $row['notification_type'] == 'reject' ) {
                $requestUser = $row['notification_request_user_id'];

                $SQL    = SQL::newSelect('approval');
                $SQL->addWhereOpr('approval_type', 'request');
                $SQL->addWhereOpr('approval_revision_id', $row['notification_rev_id']);
                $SQL->addWhereOpr('approval_entry_id', $row['notification_entry_id']);
                $SQL->addWhereOpr('approval_request_user_id', SUID);
                $SQL->addWhereOpr('approval_datetime', $row['notification_datetime'], '<');

                if ( 0
                    || !DB::query($SQL->get(dsn()), 'row')
                    || ACMS_RAM::entryStatus($row['notification_entry_id']) === 'close'
                    || ACMS_RAM::entryStatus($row['notification_entry_id']) === 'trash'
                ) {
                    continue;
                }
            }
            $count++;
        }
        return $count;
    }

    /**
     * 通知の絞り込み
     *
     * @return SQL
     */
    public function buildSql()
    {
        if ( editionIsEnterprise() ) {
            $SQL    = SQL::newSelect('workflow');
            $SQL->addSelect('workflow_type');
            $SQL->addWhereOpr('workflow_status', 'open');
            $SQL->addWhereOpr('workflow_blog_id', BID);
            $type   = DB::query($SQL->get(dsn()), 'one');

            // 並列承認
            if ( $type == 'parallel' ) {
                $SQL    = SQL::newSelect('approval_notification');
                $SQL->addLeftJoin('approval', 'notification_approval_id', 'approval_id');
                $SQL->addInnerJoin('entry_rev', 'notification_rev_id', 'entry_rev_id');
                $SQL->addWhereOpr('notification_entry_id', SQL::newField('entry_id'));

                $WHERE  = SQL::newWhere();

                // reject
                $W      = SQL::newWhere();
                $W->addWhereOpr('notification_type', 'reject', '=', 'OR');
                $WHERE->addWhere($W, 'OR');

                // request
                $W2     = SQL::newWhere();
                $W2->addWhereOpr('notification_request_user_id', SUID, '<>');
                $WHERE->addWhere($W2, 'OR');

                $SQL->addWhere($WHERE);
            // 直列承認
            } else {
                $SQL    = SQL::newSelect('usergroup_user');
                $SQL->addWhereOpr('user_id', SUID);
                $groups = DB::query($SQL->get(dsn()), 'all');
                $groupsList = array();
                foreach ( $groups as $val ) {
                    $groupsList[] = $val['usergroup_id'];;
                }

                $SQL    = SQL::newSelect('approval_notification');
                $SQL->addLeftJoin('approval', 'notification_approval_id', 'approval_id');
                $SQL->addInnerJoin('entry_rev', 'notification_rev_id', 'entry_rev_id');
                $SQL->addWhereOpr('notification_entry_id', SQL::newField('entry_id'));

                $WHERE  = SQL::newWhere();

                // reject
                $W      = SQL::newWhere();
                $W->addWhereOpr('notification_type', 'reject', '=', 'OR');
                $W->addWhereOpr('notification_receive_user_id', SUID, '=', 'OR');
                $WHERE->addWhere($W, 'OR');

                // request
                $W2     = SQL::newWhere();
                $W2->addWhereOpr('notification_receive_user_id', null);
                $W2->addWhereIn('notification_receive_usergroup_id', $groupsList);
                $WHERE->addWhere($W2, 'OR');

                $SQL->addWhere($WHERE);
            }
        } else if ( editionIsProfessional() ) {
            $SQL    = SQL::newSelect('approval_notification');
            $SQL->addLeftJoin('approval', 'notification_approval_id', 'approval_id');
            $SQL->addInnerJoin('entry_rev', 'notification_rev_id', 'entry_rev_id');
            $SQL->addWhereOpr('notification_entry_id', SQL::newField('entry_id'));

            if ( isSessionContributor(false) ) {
                $SQL->addWhereOpr('notification_type', 'request', '<>');
            }

            $WHERE  = SQL::newWhere();
            $W      = SQL::newWhere();
            $W->addWhereOpr('notification_type', 'reject', '=', 'OR');
            $W->addWhereOpr('notification_receive_user_id', SUID, '=', 'OR');
            $WHERE->addWhere($W, 'OR');

            $W2     = SQL::newWhere();
            $W2->addWhereOpr('notification_receive_user_id', null);
            $W2->addWhereOpr('notification_receive_usergroup_id', null);
            $WHERE->addWhere($W2, 'OR');

            $SQL->addWhere($WHERE);
        }

        return $SQL;
    }
}