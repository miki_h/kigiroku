<?php

namespace Acms\Services\Config;

use SQL;
use DB;
use Module;

class Import
{
    /**
     * @var array
     */
    protected $yaml;

    /**
     * @var int
     */
    protected $bid;

    /**
     * @var array
     */
    protected $meta;

    /**
     * @var array
     */
    protected $failedMeta;

    /**
     * @var
     */
    protected $failedContents;

    /**
     * @var
     */
    protected $newIDs;

    /**
     * import config from yaml
     *
     * @param int $bid
     * @param array $yaml
     *
     * @return void
     *
     * @throws \Exception
     */
    public function run($bid, $yaml)
    {
        if ( !$this->checkAuth() ) {
            die();
        }

        $this->yaml = $yaml;
        $this->bid = $bid;
        $this->failedMeta = array();

        try {
            $this->valid();
            $this->dropData();
            DB::query(SQL::optimizeSeq('module_id', dsn()), 'seq');
            DB::query(SQL::optimizeSeq('rule_id', dsn()), 'seq');
            $this->registerNewIDs();

            $tables = array(
                'rule', 'module', 'config', 'field'
            );
            foreach ( $tables as $table ) {
                $this->insertData($table);
            }

        } catch ( \Exception $e ) {
            throw $e;
        }
    }

    /**
     * @return array
     */
    public function getFailedContents()
    {
        return $this->failedContents;
    }

    /**
     * validate data
     *
     * @return bool
     */
    protected function valid()
    {
        if ( !isset($this->yaml['meta']) ) {
            return true;
        }

        foreach ( $this->yaml['meta'] as $type => $item ) {
            foreach ( $item as $id => $code ) {
                if ( $new_id = $this->getIdFromCode($type, $code) ) {
                    $this->meta[$type][$id] = $new_id;
                }
            }
        }
    }

    /**
     * drop data
     */
    protected function dropData()
    {
        // config
        if ( isset($this->yaml['config']) ) {
            $SQL = SQL::newDelete('config');
            $SQL->addWhereOpr('config_blog_id', $this->bid);
            DB::query($SQL->get(dsn()), 'exec');
        }

        // rule
        if ( isset($this->yaml['rule']) ) {
            $SQL = SQL::newDelete('rule');
            $SQL->addWhereOpr('rule_blog_id', $this->bid);
            DB::query($SQL->get(dsn()), 'exec');
        }

        // module
        if ( isset($this->yaml['module']) ) {
            $SQL = SQL::newDelete('module');
            $SQL->addWhereOpr('module_blog_id', $this->bid);
            DB::query($SQL->get(dsn()), 'exec');
        }

        // module field
        if ( isset($this->yaml['field']) ) {
            $SQL = SQL::newDelete('field');
            $SQL->addWhereOpr('field_blog_id', $this->bid);
            $SQL->addWhereOpr('field_mid', null, '<>');
            DB::query($SQL->get(dsn()), 'exec');
        }
    }

    /**
     * @return void
     */
    protected function registerNewIDs()
    {
        $tables = array(
            'rule', 'module'
        );

        foreach ( $tables as $table ) {
            $this->registerNewID($table);
        }
    }

    /**
     * @param string $table
     *
     * @return void
     */
    protected function registerNewID($table)
    {
        if ( !$this->existsYaml($table) ) {
            return;
        }
        foreach ( $this->yaml[$table] as $record ) {
            if ( !isset($record[$table . '_id']) ) {
                continue;
            }
            $id = $record[$table . '_id'];
            if ( isset($this->newIDs[$table][$id]) ) {
                continue;
            }
            $this->newIDs[$table][$id] = DB::query(SQL::nextval($table . '_id', dsn()), 'seq');
        }
    }

    /**
     * @param string $table
     *
     * @return void
     */
    protected function insertData($table)
    {
        if ( !$this->existsYaml($table) ) {
            return;
        }
        foreach ( $this->yaml[$table] as $record ) {
            $SQL = SQL::newInsert($table);
            $id = 0;
            foreach ( $record as $field => $value ) {
                if ( is_callable(array($this, $table . 'Fix')) ) {
                    $value = call_user_func_array(array($this, $table . 'Fix'), array($field, $value));
                }
                if ( $value !== false ) {
                    $SQL->addInsert($field, $value);
                }
                if ( substr($field, strlen($table . '_')) === 'id' ) {
                    $id = $value;
                }
            }
            if ( $table === 'module' ) {
                $identifier = isset($record['module_identifier']) ? $record['module_identifier'] : '';
                $scope = isset($record['module_scope']) ? $record['module_scope'] : 'local';
                if ( !Module::double($identifier, $id, $scope) ) {
                    $this->failedContents[] = array(
                        'table' => 'module',
                        'type' => 'overlap',
                        'id' => $id,
                        'identifier' => $identifier,
                    );
                    $this->failedMeta = array();
                    continue;
                }
            }
            if ( !empty($this->failedMeta) ) {
                $this->failedContents[] = array(
                    'table' => $table,
                    'type' => 'conversion',
                    'id' => $id,
                    'unlink' => $this->failedMeta,
                );
            }
            $this->failedMeta = array();

            DB::query($SQL->get(dsn()), 'exec');
        }
    }

    /**
     * Config テーブルの修正
     *
     * @param string $field
     * @param mixed $value
     *
     * @return int
     */
    protected function configFix($field, $value)
    {
        if ( $field === 'config_rule_id' ) {
            $value = $this->getNewID('rule', $value);
        } else if ( $field === 'config_module_id' ) {
            $value = $this->getNewID('module', $value);
        } else if ( $field === 'config_blog_id' ) {
            $value = $this->bid;
        }
        return $value;
    }

    /**
     * ルール テーブルの修正
     *
     * @param string $field
     * @param mixed $value
     *
     * @return int
     */
    protected function ruleFix($field, $value)
    {
        if ( $field === 'rule_id' ) {
            $value = $this->getNewID('rule', $value);
        } else if ( $field === 'rule_uid' ) {
            $value = $this->getCurrentID('uid', $value);
        } else if ( $field === 'rule_cid' ) {
            $value = $this->getCurrentID('cid', $value);
        } else if ( $field === 'rule_eid' ) {
            $value = $this->getCurrentID('eid', $value);
        } else if ( $field === 'rule_aid' ) {
            $value = $this->getCurrentID('aid', $value);
        } else if ( $field === 'rule_blog_id' ) {
            $value = $this->bid;
        }
        return $value;
    }

    /**
     * モジュール テーブルの修正
     *
     * @param string $field
     * @param mixed $value
     *
     * @return int
     */
    protected function moduleFix($field, $value)
    {
        if ( $field === 'module_id' ) {
            $value = $this->getNewID('module', $value);
        } else if ( $field === 'module_bid' ) {
            $value = $this->getCurrentID('bid', $value);
        } else if ( $field === 'module_uid' ) {
            $value = $this->getCurrentID('uid', $value);
        } else if ( $field === 'module_cid' ) {
            $value = $this->getCurrentID('cid', $value);
        } else if ( $field === 'module_eid' ) {
            $value = $this->getCurrentID('eid', $value);
        } else if ( $field === 'module_blog_id' ) {
            $value = $this->bid;
        }
        return $value;
    }

    /**
     * フィールド テーブルの修正
     *
     * @param string $field
     * @param mixed $value
     *
     * @return int
     */
    protected function fieldFix($field, $value)
    {
        if ( $field === 'field_mid' ) {
            $value = $this->getNewID('module', $value);
        } else if ( $field === 'field_blog_id' ) {
            $value = $this->bid;
        }
        return $value;
    }

    /**
     * check yaml data
     *
     * @param $table
     *
     * @return bool
     */
    protected function existsYaml($table)
    {
        if ( !isset($this->yaml[$table]) ) {
            return false;
        }
        $data = $this->yaml[$table];
        if ( !is_array($data) ) {
            return false;
        }
        return true;
    }

    /**
     * @param string $table
     * @param int $id
     *
     * @return int
     */
    protected function getNewID($table, $id)
    {
        if ( empty($id) ) {
            return null;
        }
        if ( !isset($this->newIDs[$table][$id]) ) {
            return null;
        }
        return $this->newIDs[$table][$id];
    }

    /**
     * @param string $type
     * @param int $id
     *
     * @return int
     */
    protected function getCurrentID($type, $id)
    {
        if ( empty($id) ) {
            return null;
        }
        if ( !isset($this->meta[$type][$id]) ) {
            $code = isset($this->yaml['meta'][$type][$id]) ? $this->yaml['meta'][$type][$id] : 'unknown';
            $this->failedMeta[] = array(
                'type' => $type,
                'code' => $code,
            );
            return null;
        }
        return $this->meta[$type][$id];
    }

    /**
     * get id from code
     *
     * @param string $type
     * @param string $code
     *
     * @return int | bool
     */
    protected function getIdFromCode($type, $code)
    {
        $SQL = false;
        switch ( $type ) {
            case 'bid':
                $SQL = SQL::newSelect('blog');
                $SQL->setSelect('blog_id');
                $SQL->addWhereOpr('blog_code', $code);
                break;
            case 'uid':
                $SQL = SQL::newSelect('user');
                $SQL->setSelect('user_id');
                $SQL->addWhereOpr('user_code', $code);
                $SQL->addWhereOpr('user_blog_id', $this->bid);
                break;
            case 'cid':
                $SQL = SQL::newSelect('category');
                $SQL->setSelect('category_id');
                $SQL->addWhereOpr('category_code', $code);
                $SQL->addWhereOpr('category_blog_id', $this->bid);
                break;
            case 'eid':
                $SQL = SQL::newSelect('entry');
                $SQL->setSelect('entry_id');
                $SQL->addWhereOpr('entry_code', $code);
                $SQL->addWhereOpr('entry_blog_id', $this->bid);
                break;
            case 'aid':
                $SQL = SQL::newSelect('alias');
                $SQL->setSelect('alias_id');
                $SQL->addWhereOpr('alias_domain', $code);
                $SQL->addWhereOpr('alias_code', $code);
                $SQL->addWhereOpr('alias_blog_id', $this->bid);
                break;
            default:
                return false;
                break;
        }

        $ids = DB::query($SQL->get(dsn()), 'all');
        if ( count($ids) > 1 ) {
            return false;
        }
        if ( $id = DB::query($SQL->get(dsn()), 'one') ) {
            return $id;
        }

        return false;
    }

    /**
     * check auth
     *
     * @return bool
     */
    private function checkAuth()
    {
        return sessionWithAdministration();
    }
}

