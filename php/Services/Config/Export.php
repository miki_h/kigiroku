<?php

namespace Acms\Services\Config;

use SQL;
use DB;
use ACMS_RAM;
use Symfony\Component\Yaml\Yaml;

class Export
{
    /**
     * @var array
     */
    protected $yaml;

    /**
     * @var array
     */
    protected $tables;

    /**
     * @var string
     */
    protected $metaIds;

    /**
     * Export constructor.
     */
    public function __construct()
    {
        $this->yaml = array();
        $this->meta = array();
        $this->tables = array(
            'config', 'rule', 'module'
        );
        $this->metaTargetIds = '/^(.*)_(bid|uid|cid|eid|aid)$/';
    }

    /**
     * export config data
     *
     * @param int $bid
     *
     * @return string
     */
    public function run($bid)
    {
        foreach ( $this->tables as $table ) {
            $SQL = SQL::newSelect($table);
            $SQL->addWhereOpr($table . '_blog_id', $bid);
            $q = $SQL->get(dsn());
            DB::query($q, 'fetch');
            $records = array();

            while ( $r = DB::fetch($q) ) {
                $this->extractMetaIds($r);
                $records[] = $r;
            }
            $this->setYaml($records, $table);
        }

        // field
        $SQL = SQL::newSelect('field');
        $SQL->addWhereOpr('field_blog_id', $bid);
        $SQL->addWhereOpr('field_mid', null, '<>');
        $field = DB::query($SQL->get(dsn()), 'all');
        $this->setYaml($field, 'field');

        return $this->getYaml();
    }

    /**
     * get data as array
     *
     * @return array
     */
    public function getArray()
    {
        return $this->yaml;
    }

    /**
     * get data as yaml
     *
     * @return string
     */
    public function getYaml()
    {
        return Yaml::dump($this->yaml, 2, 4);
    }

    /**
     *
     *
     * @param array $record
     */
    protected function extractMetaIds($record)
    {
        foreach ( $record as $key => $data ) {
            if ( !!$data && preg_match($this->metaTargetIds, $key, $matches) ) {
                $type = $matches[2];
                $this->yaml['meta'][$type][$data] = $this->getCode($type, $data);
            }
        }
    }

    /**
     * get code for id
     *
     * @param string $type
     * @param int $id
     *
     * @return string
     */
    protected function getCode($type, $id)
    {
        switch ( $type ) {
            case 'bid':
                return ACMS_RAM::blogCode($id);
                break;
            case 'uid':
                return ACMS_RAM::userCode($id);
                break;
            case 'cid':
                return ACMS_RAM::categoryCode($id);
                break;
            case 'eid':
                return ACMS_RAM::entryCode($id);
                break;
            case 'aid':
                return ACMS_RAM::aliasCode($id);
                break;
            default:
                break;
        }
        return false;
    }

    /**
     * set data as yaml
     *
     * @param array $records
     * @param string $table
     *
     * @return void
     */
    private function setYaml($records = array(), $table)
    {
        if ( isset($this->yaml[$table]) ) {
            $this->yaml[$table] = $records;
        } else {
            $this->yaml[$table] = $records;
        }
    }
}

