<?php

namespace Acms\Services\Database;

use Acms\Contracts\ServiceProvider;
use Acms\Services\Container;
use Acms\Services\Database\Engine;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * register service
     *
     * @param \Acms\Services\Container $container
     *
     * @return void
     */
    public function register(Container $container)
    {
        $container->singleton('db', function () {
            return Engine\PdoEngine::singleton(dsn());
//            return Engine\MysqliEngine::singleton(dsn());
        });

        $container->bind('db.replication', 'Acms\Services\Database\Replication');
    }

    /**
     * initialize service
     *
     * @return void
     */
    public function init()
    {
        \App::bootstrap('db', function ($db) {
            $q  = "SET SESSION sql_mode=''";
            $db->query($q, 'exec');
        });
    }
}