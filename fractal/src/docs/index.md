---
hidden: true
title: kigiroku components
---
kigirokuのコンポーネントライブラリ

### Component categories

  * **Global:** 全てのページに存在するグローバルなコンポーネント
  * **Layouts:** グリッドシステムなどのレイアウトを担うコンポーネント
  * **Objects:** どこでも使用できるコンポーネント
  * **Projects:** Objectsにそれぞれ特有のスタイルをつけるときのコンポーネント
  * **Templates:** テンプレート
