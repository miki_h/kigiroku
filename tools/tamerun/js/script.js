$(function() {

	// 内容がwindowの高さより小さい場合にのみfooterを下に固定する
    var w = $(window).height();
    var x = $('html').height();
    if (w >= x) {
        $('footer').css({
            position: 'fixed',
            bottom: '0',
            width: '100%'
        });
    }

    // グローバルナビゲーション現在地をactiveにする
    if(location.pathname != "/" ) {
        var index = $('nav a[href^="/tamerun/' + location.pathname.split("/")[2] + '"]').index('nav a');
        $('nav li').eq(index).addClass('active');
    }

    $("#datepicker").datepicker({
        dateFormat: 'yy/mm/dd'
    });
    $("#datepicker").datepicker("option", "showOn", 'button');
    $("#datepicker").datepicker("option", "buttonImageOnly", true);
    $("#datepicker").datepicker("option", "buttonImage", '/tamerun/images/parts/ico_calendar.png');

    $('button.fc-prev-button').html('<img src="/tamerun/images/parts/prev_btn.png">');
    $('button.fc-next-button').html('<img src="/tamerun/images/parts/next_btn.png">');

    $(".js-offcanvas").hiraku({
        btn: ".js-offcanvas-btn",
        fixedHeader: ".js-fixed-header",
        direction: "right",
        breakpoint: 767
    });


});