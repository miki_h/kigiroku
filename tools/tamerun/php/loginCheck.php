<?php
	session_start();

	// ログイン状態チェック
	if (!isset($_SESSION["USERID"])) {
	    header("Location: /");
	    exit;
	}
?>