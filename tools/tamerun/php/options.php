<?php header('Content-Type: text/css; charset=utf-8'); ?>
@charset "UTF-8";


<?php
	
	session_start();

    require_once "../dbconnect.php";

    $name = $_SESSION["USERID"];

    $stmt = $pdo->query("SELECT * FROM `option` WHERE `name` = '$name'");

    foreach ($stmt as $row) {
        $colorcode = $row['maincolor'];

        // echo $row['maincolor'];
        // echo '!!!!!';
    }

  //   if(){
		// //「#******」のような形でカラーコードがわたってきた場合「#」を削除する
		// $colorcode = preg_replace("/#/", "", $colorcode);
		 
		// //「******」という形になっているはずなので、2つずつ「**」に区切る
		// //そしてhexdec関数で変換して配列に格納する
		// $r = hexdec(substr($colorcode, 0, 2));
		// $g = hexdec(substr($colorcode, 2, 2));
		// $b = hexdec(substr($colorcode, 4, 2));
  //   }else if(){
  //   	$r = 129;
		// $g = 247;
		// $b = 159;
  //   }

    //「#******」のような形でカラーコードがわたってきた場合「#」を削除する
		$colorcode = preg_replace("/#/", "", $colorcode);
		 
		//「******」という形になっているはずなので、2つずつ「**」に区切る
		//そしてhexdec関数で変換して配列に格納する
		$r = hexdec(substr($colorcode, 0, 2));
		$g = hexdec(substr($colorcode, 2, 2));
		$b = hexdec(substr($colorcode, 4, 2));
?>

.btn {
	background-color: <?php echo 'rgba('.$r.','.$g.','.$b.',0.6)'; ?>;
	border: 3px solid <?php echo 'rgba('.$r.','.$g.','.$b.',0.6)'; ?>;
}

button#hiraku-offcanvas-btn-1 {
    background-color: <?php echo 'rgba('.$r.','.$g.','.$b.',1)'; ?>;
}

a.list-group-item:last-child {
    background-color: <?php echo 'rgba('.$r.','.$g.','.$b.',1)'; ?>;
}

@media screen and (max-width: 520px){

	header {
	    border-bottom: 6px solid <?php echo 'rgba('.$r.','.$g.','.$b.',1)'; ?>;
	}

}

header nav {
	background-color: <?php echo 'rgba('.$r.','.$g.','.$b.',0.6)'; ?>;
}

header nav ul li:hover,
header nav ul li.active {
	background-color: <?php echo 'rgba('.$r.','.$g.','.$b.',1)'; ?>;
}

main div.saving-edit-item {
	border-top: 5px solid <?php echo 'rgba('.$r.','.$g.','.$b.',0.6)'; ?>;
	border-bottom: 5px solid <?php echo 'rgba('.$r.','.$g.','.$b.',0.6)'; ?>;
}
main div.saving-edit-item li {
	border-right: 2px solid <?php echo 'rgba('.$r.','.$g.','.$b.',0.6)'; ?>;
}

.fc-unthemed .fc-today {
	background: <?php echo 'rgba('.$r.','.$g.','.$b.',0.6)'; ?>;
}

footer {
	background-color: <?php echo 'rgba('.$r.','.$g.','.$b.',0.6)'; ?>;
}
