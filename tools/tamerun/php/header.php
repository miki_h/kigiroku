<header class="cf">
	<div class="container">
		<h1><a href="/tamerun/mypage"><img src="/tamerun/images/logo.png"></a></h1>
		<p class="user-name">
			<?php echo $_SESSION["USERID"]; ?>
			<a href="/tamerun/logout.html" class="mobile-none">ログアウト</a>
		</p>
	</div>
	<nav class="cb">
		<div class="container">
			<ul class="cf">
				<li><a href="/tamerun/shopping/">買い物メモ</a></li>
				<!-- <li><a href="/tamerun/buy/">購入リスト</a></li> -->
				<li><a href="/tamerun/want/">欲しい物</a></li>
				<li><a href="/tamerun/saving/">貯める</a></li>
				<li><a href="/tamerun/mypage/">マイページ</a></li>
			</ul>
		</div>
	</nav>
	<button type="button" class="navbar-toggle collapsed js-offcanvas-btn">
        <span class="hiraku-open-btn-line"></span>
    </button>
</header>