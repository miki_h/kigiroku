<?php

	require_once "../dbconnect.php";

	$id = $_GET['id'];

	$sql = 'DELETE FROM `want` where `id` = :id';
	$stmt = $pdo -> prepare($sql);
	$stmt -> bindParam(':id', $id, PDO::PARAM_INT);
	$stmt -> execute();

	header('Location: /tamerun/want/');

?>
