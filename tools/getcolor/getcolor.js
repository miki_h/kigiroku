	var canvas = document.getElementById("color");	
	var dCanrun = false;	//稼働可能かどうか。対応してないブラウザなどはこの値がfalseになる
	var dInFile = false;	//最初の画像ファイルを読み込んだかどうか。この値がtrueでない場合は、設定変更されても画像プレビューを出さない
	var dProcing= false;	//画像処理中。他の処理に割り込まないようにするためのロック。
	var dLoadType = 0;		//読み込む画像の種類
	var dLastValueU = "";	//テキスト変更確認
	var dLastValueD = "";	//テキスト変更確認
	
	var dIntervalAdd;
	var dLastTime;				//最終処理時間
	var dNowTime;
	
	var rColorCount;
	var rCols = new Array(5);
	var rRGBs = new Array(5);
	var rHexs = new Array(5);
	
	var rInFileBox;
	
	var dOpenFileName = "";
	
	var canvas_ori = new Image;
	var rImageData;	//ピクセルデータ
	var rPixelData;	//ピクセルデータ
	
	
	var rOriRead;
	var cnt;
	if (canvas && canvas.getContext && window.File && window.FileReader && window.FileList && window.Blob) {
		//***** ブラウザが File API に完全に対応しているかを確認 *****
		// document.getElementById("inojs").innerHTML = '';
		document.getElementById("inimgs").style.backgroundImage = 'url(http://www.peko-step.com/image/softlist10_b.png)';
		
		rInFileBox = document.getElementById("pInFileBox");
		
		rOriRead = new FileReader();
		rOriRead.onload = funcOriLoad;
		
		cnt = canvas.getContext("2d");	//キャンバスの2Dコンテキストを取得
		dCanrun = true;
		
		var dViewImg = document.getElementById("dprvimg");
		if (dViewImg.addEventListener){		//イベントリスナーに対応している場合の処理
			if (navigator.userAgent.indexOf('iPhone') >= 0 || navigator.userAgent.indexOf('iPad') >= 0 || navigator.userAgent.indexOf('iPod') >= 0 || navigator.userAgent.indexOf('Android') >= 0) {
				//***** スマートフォンの場合 *****
			}
			else {
				//***** PCの場合 *****
				rInFileBox.addEventListener("dragover",funcFileDragOver, false);
				rInFileBox.addEventListener("dragenter",funcFileDragOver, false);
				rInFileBox.addEventListener("drop",funcFileDrop, false);
				
				dViewImg.addEventListener("mousedown",funcMouseDown, false);
				dViewImg.addEventListener("mousemove",funcMouseMove, false);
			}
		}
		else {
			//イベントリスナーに対応していない場合(IE)の処理
			rInFileBox.attachEvent("ondragover" ,funcFileDragOver);
			rInFileBox.attachEvent("ondragenter" ,funcFileDragOver);
			rInFileBox.attachEvent("ondrop" ,funcFileDrop);
			
			dViewImg.attachEvent("onmousedown" ,funcMouseDown);
			dViewImg.attachEvent("onmousemove" ,funcMouseMove);
		}
	}
	else {
		dCanrun = false;
		document.getElementById("inojs").innerHTML = 'このブラウザでは動作しません。';
	}

	function funcOriLoad(evt) {
		//***** ファイルリーダー。画像を読み終わったら、canvas_oriに画像を保存 *****
		canvas_ori.src = evt.target.result;
		dIntervalAdd = setInterval("funcImgLoad()",50);
	}

	function funcFileClear(ffc_type) {
		//***** ファイルを開くボタンが押されたらファイル名を消す。これを入れておかないとonchangeが発生しない *****
		document.getElementById('dfilename').value = "";
	}
	
	function funcFileChange(event) {
		//***** 加工する画像ファイルを読み込む *****
		if (dCanrun == false) {
			//***** ブラウザが動作に対応していない場合 *****
			alert('このブラウザでは動作しません。');
			dProcing = false;
			return;
		}
		
		var dFile = event.target.files[0];	//選択された File オブジェクトのリストからファイルを取り出し。ファイルはひとつしか使わないので先頭のみを取り出す。
		if (!dFile.type.match(/^image\/(png|jpeg|gif|bmp)$/)) {
			//***** 画像ファイル以外だったら処理を終了する *****
			alert('処理できるのは画像ファイルのみです。');
			dProcing = false;
			return;
		}
		
		if (dProcing == true) {
			//***** 10秒以上処理中なら、処理を強制解除する *****
			dNowTime = new Date();
			if (dNowTime.getTime() - dLastTime.getTime() >= 20000) {
				dProcing = false;
			}
		}
		
		//***** 画像ファイルを読み込み、Base64でエンコードされたデータURLとして返す。 *****
		if (dProcing == false) {
			//***** 他の処理に割り込まないようにブロッキング *****
			dProcing = true;
			
			document.getElementById("vwbox").style.display = "none";	//開いている最中にリセットボタンを押されないように
			if (dOpenFileName == document.getElementById('dfilename').value) {
				//***** 同じファイルを開く場合はOnLoadが発生しないので読み込み過程を省く *****
				funcImgLoad();
			}
			else {
				dOpenFileName = document.getElementById('dfilename').value;
				rOriRead.readAsDataURL(dFile);
			}
		}
		else {
			alert('処理中です。20秒ほどお待ちください。');
		}
	}

	function funcImgLoad(){
		//***** 画像読み込みまで待機する処理 *****
		clearInterval(dIntervalAdd);
		
		//funcSetCol();
		canvas.width = canvas_ori.naturalWidth;
		canvas.height = canvas_ori.naturalHeight;
		cnt.drawImage(canvas_ori, 0, 0, canvas_ori.naturalWidth,canvas_ori.naturalHeight);
		document.getElementById("dprvimg").src = canvas.toDataURL();
		rImageData = cnt.getImageData(0, 0, canvas.width, canvas.height);
		rPixelData = rImageData.data;
		
		document.getElementById("dprvimg").style.visibility = "visible";
		document.getElementById("vwbox").style.display = "block";
		
		dProcing = false;
	}

	function funcSetCol(){
		var x,y;
		var dAlp;
		var dR,dG,dB,dTop,dMax,dMin;
		var dEndH,dEndS,dEndV;
		var dH,dS,dV;
		var dEndR,dEndG,dEndB;
		var drHi,drF,drp,drq,drt;
		
		canvas.width = canvas_ori.naturalWidth;
		canvas.height = canvas_ori.naturalHeight;
		cnt.drawImage(canvas_ori, 0, 0, canvas_ori.naturalWidth,canvas_ori.naturalHeight);
		
		rImageData = cnt.getImageData(0, 0, canvas.width, canvas.height);
		rPixelData = rImageData.data;
		
		
		
		for (x=0;x<rImageData.width;x++) {
			for (y=0;y<rImageData.height;y++) {
				dAlp = Math.floor(rPixelData[(((y * rImageData.width) + x) * 4) + 0] * 0.2126 + rPixelData[(((y * rImageData.width) + x) * 4) + 1] * 0.7152 + rPixelData[(((y * rImageData.width) + x) * 4) + 2] * 0.0722);
				if (dAlp < 0) {
					dAlp = 0;
				}
				if (dAlp > 255) {
					dAlp = 255;
				}
				
				dEndH = document.getElementById('phval').value;
				dH = dEndH / 60;
				dS = 0.25;
				dV = dAlp / 255;
				dV += 0.1;
				if (dV > 1) {
					dV = 1;
				}
				
				
				if (dS == 0) {
					//***** 灰色 *****
					dEndR = Math.floor(dV * 255);
					if (dEndR > 255) {
						dEndR = 255;
					}
					else if (dEndR < 0) {
						dEndR = 0;
					}
					dEndG = dEndR;
					dEndB = dEndR;
				}
				else {
					drHi = Math.floor(dH) % 6;	//modは数値が6以上にならないようにするため
					drF  = dH - Math.floor(dH);
					drp  = dV * (1 - dS);
					drq  = dV * (1 - dS * drF);
					drt  = dV * (1 - dS * (1 - drF));
					
					if (drHi == 0) {
						dEndR = dV;
						dEndG = drt;
						dEndB = drp;
					}
					else if (drHi == 1) {
						dEndR = drq;
						dEndG = dV;
						dEndB = drp;
					}
					else if (drHi == 2) {
						dEndR = drp;
						dEndG = dV;
						dEndB = drt;
					}
					else if (drHi == 3) {
						dEndR = drp;
						dEndG = drq;
						dEndB = dV;
					}
					else if (drHi == 4) {
						dEndR = drt;
						dEndG = drp;
						dEndB = dV;
					}
					else {
						dEndR = dV;
						dEndG = drp;
						dEndB = drq;
					}
					
					dEndR = Math.floor(dEndR * 255);
					if (dEndR > 255) {
						dEndR = 255;
					}
					else if (dEndR < 0) {
						dEndR = 0;
					}
					dEndG = Math.floor(dEndG * 255);
					if (dEndG > 255) {
						dEndG = 255;
					}
					else if (dEndG < 0) {
						dEndG = 0;
					}
					dEndB = Math.floor(dEndB * 255);
					if (dEndB > 255) {
						dEndB = 255;
					}
					else if (dEndB < 0) {
						dEndB = 0;
					}
				}
				
				rPixelData[(((y * rImageData.width) + x) * 4) + 0] = dEndR;
				rPixelData[(((y * rImageData.width) + x) * 4) + 1] = dEndG;
				rPixelData[(((y * rImageData.width) + x) * 4) + 2] = dEndB;
			}
		}
		
		cnt.putImageData(rImageData, 0, 0);
		
		document.getElementById("dprvimg").src = canvas.toDataURL();
	}
	
	function funcFileDrop(e) {
		//***** 編集画像ファイルをドロップで受け取った場合の処理 *****
		e.stopPropagation();
		e.preventDefault();
		
		dLoadType=0;
		
		var dFile = e.dataTransfer.files[0];
		if (!dFile.type.match(/^image\/(png|jpeg|gif|bmp)$/)) {
			//***** 画像ファイル以外だったら処理を終了する *****
			alert('処理できるのは画像ファイルのみです。');
			dProcing = false;
			return;
		}
		
		if (dProcing == true) {
			//***** 10秒以上処理中なら、処理を強制解除する *****
			dNowTime = new Date();
			if (dNowTime.getTime() - dLastTime.getTime() >= 20000) {
				dProcing = false;
			}
		}
		
		//***** 画像ファイルを読み込み、Base64でエンコードされたデータURLとして返す。 *****
		if (dProcing == false) {
			//***** 他の処理に割り込まないようにブロッキング *****
			dProcing = true;
			
			document.getElementById("vwbox").style.display = "none";	//開いている最中にリセットボタンを押されないように
			//***** ドロップではファイル名を正確に取得できないので *****
			dOpenFileName = "";
			rOriRead.readAsDataURL(dFile);
		}
		else {
			alert('処理中です。20秒ほどお待ちください。');
		}
	}
	function funcFileDragOver(e) {
		//***** dropイベントはdragoverイベントとdragenterイベントをキャンセルしないと受け付けてくれない *****
		e.stopPropagation();
		e.preventDefault();
		e.dataTransfer.dropEffect = "copy";
	}

	function funcMouseDown(e) {
		//***** マウスダウンされた場合の処理 *****
		//IE以外のブラウザでは引数にイベントオブジェクトが自動的に渡される。なのでこれから値を参照する
		
		var fmdX,fmdY,fmdS;
		var fmdProcs = false;
		var dR,dG,dB,dA,dColTex,dRGBTex,dHexTex;
		
		//***** クリック座標原点の取得。ずれる可能性を考えて毎ターン再取得する *****
		var bounds = document.getElementById("dprvimg").getBoundingClientRect();
		//scrollLeftは標準モードと互換モードで取得方法が違うので両方に対応した取得方法を使っている。
		rContX = bounds.left + (document.body.scrollLeft || document.documentElement.scrollLeft);
		rContY = bounds.top + (document.body.scrollTop || document.documentElement.scrollTop);
			 
		if (e) {
			//***** IE以外の処理 *****
			fmdX = Math.floor(e.pageX - rContX);
			fmdY = Math.floor(e.pageY - rContY);
			if (e.button <= 1) {
				//***** 右クリックの時は処理しない *****
				fmdProcs = true;
			}
		}
		else {
			//IEの場合はeventという名のイベントオブジェクトが別に定義されているのでそこから参照する。スクロール値はIE9以前と以降で算出が違うので注意
			fmdX = Math.floor(event.clientX + (document.body.scrollLeft || document.documentElement.scrollLeft) - rContX);
			fmdY = Math.floor(event.clientY + (document.body.scrollTop || document.documentElement.scrollTop) - rContY);
			if (event.button <= 1) {
				//***** 右クリックの時は処理しない *****
				fmdProcs = true;
			}
		}
		
		if (fmdX >= 0 && fmdX < rImageData.width && fmdY >= 0 && fmdY < rImageData.height) {
			var dR = rPixelData[(((fmdY * rImageData.width) + fmdX) * 4) + 0];
			var dG = rPixelData[(((fmdY * rImageData.width) + fmdX) * 4) + 1];
			var dB = rPixelData[(((fmdY * rImageData.width) + fmdX) * 4) + 2];
			var dA = rPixelData[(((fmdY * rImageData.width) + fmdX) * 4) + 3];
			
			
			dRGBTex = 'rgb(' + dR + ',' + dG + ',' + dB + ')';
			
			fncN = (1 << 24) | (dR << 16) | (dG << 8) | dB;
			fncT = fncN.toString(16);
			fncT = fncT.substr(1,6);
			dColTex = '#' + fncT.toUpperCase();
			
			dHexTex = 'rgba(' + dR + ',' + dG + ',' + dB + ',1.0)';
			
			funcAddCol(dColTex,dRGBTex,dHexTex);
		}
	}
	
	function funcMouseMove(e) {
		//***** マウスカーソル移動の処理。 *****
		//IE以外のブラウザでは引数にイベントオブジェクトが自動的に渡される。なのでこれから値を参照する
		var fmdX,fmdY;
		var fncN,fncT;
		
		//***** クリック座標原点の取得。ずれる可能性を考えて毎ターン再取得する *****
		var bounds = document.getElementById("dprvimg").getBoundingClientRect();
		//scrollLeftは標準モードと互換モードで取得方法が違うので両方に対応した取得方法を使っている。
		rContX = bounds.left + (document.body.scrollLeft || document.documentElement.scrollLeft);
		rContY = bounds.top + (document.body.scrollTop || document.documentElement.scrollTop);
		
		if (e) {
			//***** IE以外の処理 *****
			fmdX = Math.floor(e.pageX - rContX);
			fmdY = Math.floor(e.pageY - rContY);
		}
		else {
			//IEの場合はeventという名のイベントオブジェクトが別に定義されているのでそこから参照する。スクロール値はIE9以前と以降で算出が違うので注意
			fmdX = Math.floor(event.clientX + (document.body.scrollLeft || document.documentElement.scrollLeft) - rContX);
			fmdY = Math.floor(event.clientY + (document.body.scrollTop || document.documentElement.scrollTop) - rContY);
		}
		
		if (fmdX >= 0 && fmdX < rImageData.width && fmdY >= 0 && fmdY < rImageData.height) {
			var dR = rPixelData[(((fmdY * rImageData.width) + fmdX) * 4) + 0];
			var dG = rPixelData[(((fmdY * rImageData.width) + fmdX) * 4) + 1];
			var dB = rPixelData[(((fmdY * rImageData.width) + fmdX) * 4) + 2];
			var dA = rPixelData[(((fmdY * rImageData.width) + fmdX) * 4) + 3];
			
			//document.getElementById('oncoltext').innerHTML = 'R:' + dR + ' ,G:' + dG + ' ,B:' + dB + ' ,A:' + dA;
			document.getElementById('dtextrgb').innerHTML = 'rgb(' + dR + ',' + dG + ',' + dB + ')';
			
			fncN = (1 << 24) | (dR << 16) | (dG << 8) | dB;
			fncT = fncN.toString(16);
			fncT = fncT.substr(1,6);
			document.getElementById('dtextcol').innerHTML = '#' + fncT.toUpperCase();
			
			document.getElementById("dcolview").style.backgroundColor = 'rgba(' + dR + ',' + dG + ',' + dB + ',1.0)';
		}
	}





function funcAddCol(fAC_Col,fAC_RGB,fAC_Hex) {
	//***** 初期化処理 *****
	var i;
	if (rColorCount == 0) {
		rCols[0] = fAC_Col;
		rRGBs[0] = fAC_RGB;
		rHexs[0] = fAC_Hex;
		
		document.getElementById("dcolview0").style.backgroundColor = rHexs[0];
		document.getElementById("dtextcol0").innerHTML = rCols[0];
		document.getElementById("dtextrgb0").innerHTML = rRGBs[0];
		
		document.getElementById("dcolboxc0").style.display = "table-row";
		document.getElementById("dcolboxb0").style.display = "table-row";
		document.getElementById("dcolboxa0").style.display = "table-row";
		
		rColorCount++;
	}
	else {
		for (i=4;i>=1;i--) {
			rCols[i] = rCols[i - 1];
			rRGBs[i] = rRGBs[i - 1];
			rHexs[i] = rHexs[i - 1];
		}
		rCols[0] = fAC_Col;
		rRGBs[0] = fAC_RGB;
		rHexs[0] = fAC_Hex;
		
		rColorCount++;
		if (rColorCount > 5) {
			rColorCount = 5;
		}
		
		for (i=0;i<rColorCount;i++) {
			document.getElementById("dcolview" + i).style.backgroundColor = rHexs[i];
			document.getElementById("dtextcol" + i).innerHTML = rCols[i];
			document.getElementById("dtextrgb" + i).innerHTML = rRGBs[i];
			
			document.getElementById("dcolboxc" + i).style.display = "table-row";
			document.getElementById("dcolboxb" + i).style.display = "table-row";
			document.getElementById("dcolboxa" + i).style.display = "table-row";
		}
	}
	
	
}

function funcClear() {
	//***** 初期化処理 *****
	var i;
	
	rColorCount = 0;
	for (i=0;i<5;i++) {
		rCols[i] = "";
		rRGBs[i] = "";
		rHexs[i] = "";
		
		document.getElementById("dcolboxc" + i).style.display = "none";
		document.getElementById("dcolboxb" + i).style.display = "none";
		document.getElementById("dcolboxa" + i).style.display = "none";
	}
}

funcClear();








