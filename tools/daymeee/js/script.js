$(function(){
	setInterval(function(){
		var now = new Date();
		hou = toDD(now.getHours());
		min = toDD(now.getMinutes());
		sec = toDD(now.getSeconds());
		$('.clock').text(hou +":"+ min);
	},1000);

	var toDD = function(num){
		num += "";
		if(num.length === 1){num = "0" + num;}
		return num;
	}

	    var now_hour = new Date().getHours();

	     if ( 5 <= now_hour && now_hour <= 7 ){
	        $("body").css("background-image","url('/images/bg/bg-m.jpg')");
	        $("body").css("background-repeat","repeat-x");
	    } else if ( 8 <= now_hour && now_hour <= 16 ) {
	        $("body").css("background-image","url('/images/bg/bg-n.jpg')");
	        $("body").css("background-repeat","repeat-x");
	    } else if ( 17 <= now_hour && now_hour <= 18) {
	        $("body").css("background-image","url('/images/bg/bg-m.jpg')");
	        $("body").css("background-repeat","repeat-x");
	    } else if ( 19 <= now_hour && now_hour <= 24) {
	        $("body").css("background-image","url('/images/bg/bg-o.jpg')");
	        $("body").css("background-repeat","repeat-x");
	    } else if ( 0 <= now_hour && now_hour <= 4) {
	        $("body").css("background-image","url('/images/bg/bg-o.jpg')");
	        $("body").css("background-repeat","repeat-x");
	    }

	$('#s-time').click(function() {

		hTime = new Date().getHours();
		mTime = new Date().getMinutes();
		var time = $('input[name="time"]').val();
		var split = time.split(':');
		var sleep_hTime = split[0];
		var sleep_mTime = split[1];
		$('h2 img').attr('src','/images/headline/headline-foodTime.png');
		$('.re-write').html('<p class="sleep-time"><img src="images/text/text-commentToday.png" alt="今日は" height="15"><span class="sleepTime"></span>'+
							'<img src="/images/text/text-commentSleep.png" alt="寝ました" height="15"></p>'+
							'<p class="food-time"><img src="/images/text/text-riceTime.png" height="15" alt="ご飯の時間は"><br>'+
							'<img src="/images/text/text-foodMorning.png" height="20" alt="朝" style="vertical-align: top;"><span class="f-m"></span>'+
							'<img src="/images/text/text-foodAfternoon.png" height="20" alt="昼" style="vertical-align: top;"><span class="f-n"></span>'+
							'<img src="/images/text/text-foodNight.png" height="20" alt="夜" style="vertical-align: top;"><span class="f-o"></span>'+
							'<img src="/images/text/text-osusumedayou.png" height="15" alt="頃がおすすめだよ！"></p>'+
                			'<a href="cheak.html"><img src="/images/btn/btn-next.png" width="80" class="btn" alt="次へ" style="padding-top:20px"></a>'
                			);

	if( sleep_hTime > hTime && sleep_mTime < mTime){
		h = 24-sleep_hTime+hTime;
		m = mTime-sleep_mTime;

		if( String(m).length == 1){
			m = '0' + m ;
			result = h + '時間' + m + '分';
			$('.sleepTime').text(result);
			$('.f-m').text('~'+ (hTime+1) + ':' + m );
			$('.f-n').text((hTime+6) + ':' + m );
			$('.f-o').text((hTime+11) + ':' + m );
		}else{
			result = h + '時間' + m + '分';
			$('.sleepTime').text(result);
			$('.f-m').text('~'+ (hTime+1) + ':' + m );
			$('.f-n').text((hTime+6) + ':' + m );
			$('.f-o').text((hTime+11) + ':' + m );
		}
		
	}

	if( sleep_hTime > hTime && sleep_mTime > mTime){
		h = 24-sleep_hTime+hTime-1;
		m = 60-sleep_mTime+mTime;
		
		if( String(m).length == 1){
			m = '0' + m ;
			result = h + '時間' + m + '分';
			$('.sleepTime').text(result);
			$('.f-m').text('~'+ (hTime+1) + ':' + m );
			$('.f-n').text((hTime+6) + ':' + m );
			$('.f-o').text((hTime+11) + ':' + m );
		}else{
			result = h + '時間' + m + '分';
			$('.sleepTime').text(result);
			$('.f-m').text('~'+ (hTime+1) + ':' + m );
			$('.f-n').text((hTime+6) + ':' + m );
			$('.f-o').text((hTime+11) + ':' + m );
		}
	
	}

	if( sleep_hTime < hTime && sleep_mTime < mTime){
		h = hTime-sleep_hTime;
		m = mTime-sleep_mTime;
		
		if( String(m).length == 1){
			m = '0' + m ;
			result = h + '時間' + m + '分';
			$('.sleepTime').text(result);
			$('.f-m').text('~'+ (hTime+1) + ':' + m );
			$('.f-n').text((hTime+6) + ':' + m );
			$('.f-o').text((hTime+11) + ':' + m );
		}else{
			result = h + '時間' + m + '分';
			$('.sleepTime').text(result);
			$('.f-m').text('~'+ (hTime+1) + ':' + m );
			$('.f-n').text((hTime+6) + ':' + m );
			$('.f-o').text((hTime+11) + ':' + m );
		}
	
	}

	if( sleep_hTime < hTime && sleep_mTime > mTime){
		h = hTime-sleep_hTime-1;
		m = 60-sleep_mTime+mTime;
		
		if( String(m).length == 1){
			m = '0' + m ;
			result = h + '時間' + m + '分';
			$('.sleepTime').text(result);
			$('.f-m').text('~'+ (hTime+1) + ':' + m );
			$('.f-n').text((hTime+6) + ':' + m );
			$('.f-o').text((hTime+11) + ':' + m );
		}else{
			result = h + '時間' + m + '分';
			$('.sleepTime').text(result);
			$('.f-m').text('~'+ (hTime+1) + ':' + m );
			$('.f-n').text((hTime+6) + ':' + m );
			$('.f-o').text((hTime+11) + ':' + m );
		}
	
	}

	});

	var list_length = $('.recipe-list li').length;
	if( list_length < 8 && list_length != 0){
		$('footer').css('position','absolute');
		$('footer').css('bottom','0');
	}



});