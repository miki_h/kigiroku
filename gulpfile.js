/**
 *  *1・*2 gulp-csscombとgulp-sourcemaps は、gulp-csscombがgulp-sourcemapsに対応していないので、同時に動作することができません。
 *  gulp-csscombを動作させたい場合は、「*2」の記述をコメントアウトしてください。
 */

const gulp = require('gulp');
const rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    path = require('path'),
    cssnano = require('gulp-cssnano'),
    watch = require('gulp-watch'),
    autoprefixer = require('gulp-autoprefixer'),
    csscomb = require('gulp-csscomb'),
    replace = require('gulp-replace'),
    sassJson = require('gulp-sass-json'),
    changed = require('gulp-changed'),
    package = require('./package.json');

const rootTheme = package.config.theme;

const imagesSrc = `${rootTheme}images/**`;
const imagesDest = 'fractal/tmp/images';

const destSrc = `${rootTheme}dest/**`;
const destDest = 'fractal/tmp/dest';

const fontsSrc = `${rootTheme}fonts/**`;
const fontsDest = 'fractal/tmp/fonts';

//SCSSファイルをCSSにコンパイルする
gulp.task('bootstrapsass', function () {
    gulp.src(['themes/' + rootTheme + '/src/scss/foundation/bootstrap/bootstrap.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 version'],
            cascade: false,
            remove: false
        }))
        .pipe(csscomb())// *1
        .pipe(gulp.dest('themes/' + rootTheme + '/dest'))
        .pipe(gulp.dest('fractal/tmp/dest'));
});
// SCSSファイルを圧縮する
gulp.task('bootstrapmin', function () {
    gulp.src(['themes/' + rootTheme + '/dest/bootstrap.css'])
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(csscomb())// *1
        .pipe(cssnano({safe: true}))
        .pipe(gulp.dest('themes/' + rootTheme + '/dest'))
         .pipe(gulp.dest('fractal/tmp/dest'));
});

gulp.task('design-tokens', function () {
  return gulp
    .src('themes/' + rootTheme + '/src/scss/foundation/tokens/*.scss')
    .pipe(sassJson())
    .pipe(gulp.dest('./fractal/src/docs/tokens/'));
});

//テーマ
//SCSSファイルをCSSにコンパイルする
gulp.task('sass', function () {
    gulp.src(['themes/' + rootTheme + '/src/scss/site.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(csscomb())// *1
        .pipe(rename({
            basename: 'bundle'
        }))
        .pipe(gulp.dest('themes/' + rootTheme + '/dest'))
        .pipe(gulp.dest('fractal/tmp/dest'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cssnano({safe: true}))
        .pipe(gulp.dest('themes/' + rootTheme + '/dest'))
        .pipe(gulp.dest('fractal/tmp/dest'));
});

gulp.task('changed', function() {
  gulp.src(['themes/' + rootTheme + '/images/**'])
    .pipe(gulp.dest(imagesDest));
  gulp.src(['themes/' + rootTheme + '/fonts/**'])
    .pipe(gulp.dest(fontsDest));
});


// プロジェクトのSCSSとCSSファイルを監視する
gulp.task('project', function() {
  gulp.watch('themes/' + rootTheme + '/src/scss/foundation/tokens/**', ['design-tokens']);
  gulp.watch('themes/' + rootTheme + '/src/scss/**',['sass']);
  gulp.watch(['themes/' + rootTheme + '/images/**','themes/' + rootTheme + '/fonts/**'], ['changed']);
});

gulp.task('bootstrap', function() {
    gulp.watch('themes/' + rootTheme + '/src/scss/foundation/bootstrap/**',['bootstrapsass']);
    gulp.watch('themes/' + rootTheme + '/dest/bootstrap.css',['bootstrapmin']);
});

// デフォルトのタスク
gulp.task('default', ['project','bootstrap']);
