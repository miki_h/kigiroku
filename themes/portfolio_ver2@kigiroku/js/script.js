$(function() {
    $('nav.gnav li a').each(function(){
        var $href = $(this).attr('href');
        if(location.href.match($href)) {
            $(this).addClass('active');
        } else {
        $(this).removeClass('active');
        }
    });

    var $setFilter = $('#filter'),
    $setList = $('#filterlist'),
    $setFilterAll = $('.allitem');
 
    var $setFilterBtn = $setFilter.children('a'),
    $setFilterList = $setList.children('ul').children('li'),
    $filterAllItem = $setFilterAll.attr('class');
 
    $setFilterBtn.click(function(){
        if(!($(this).hasClass('active'))){
            var filterClass = $(this).attr('class');
            $setFilterList.each(function(){
                if($(this).hasClass(filterClass)){
                    $(this).css({display:'block'});
                    $(this).find('*').css({opacity:'1'});
                    $(this).css({width:'33%'});
                } else {
                    $(this).css({display:'none'})
                    $(this).find('*').css({opacity:'0'});
                    $(this).css({width:'0'},function(){
                        $(this).css({display:'none'});
                    });
                }
            });
            $setFilterBtn.removeClass('active');
            $(this).addClass('active');
        }
    });
 
    $setFilterAll.click(function(){
        $setFilterList.each(function(){
            $(this).css({display:'block'});
            $(this).find('*').css({opacity:'1'});
            $(this).css({width:'33%'});
        });
    });
    $setFilterAll.click();

    $('select#filter').change(function() {
        var val = $(this).val();
        $('a.'+val).click();
    });

    var ww = $(window).height();
    var www = $('body').height();
    if(www <= ww){
        $('footer').css('position','fixed');
        $('footer').css('width','100%');
    }

   
});

