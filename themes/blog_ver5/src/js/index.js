$(function () {
    // ヘッダー固定
    var $header = $('.js-header');
    var offset = $('header').height();
    $header.addClass('header-fixed').insertAfter($header);
    $(window).scroll(function () {
        var scroll_top = $(window).scrollTop();
        if (!$header.hasClass('is-fixed') && scroll_top > offset) {
            $header.addClass('is-fixed');
        } else if ($header.hasClass('is-fixed') && scroll_top <= offset) {
            $header.removeClass('is-fixed');
        }
    });

    // #で始まるアンカーをクリックした場合に処理
    $('a').click(function() {
        // スクロールの速度
        var speed = 400; // ミリ秒
        // アンカーの値取得
        var href= $(this).attr("href");
        // 移動先を取得
        var target = $(href == "#" || href == "" ? 'html' : href);
        // 移動先を数値で取得
        var position = target.offset().top;
        // スムーススクロール
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });

    // 目次
    //$('.c-agenda__item.-h3').wrapAll('<ul />');

    $( ".c-global-nav li:contains('a-blog cms')" ).prepend('<span class="acms-admin-icon acms-admin-icon-logo"></span>');
    $( ".c-global-nav li:contains('Analytics')" ).prepend('<i class="fa fa-area-chart" aria-hidden="true"></i>');
    $( ".c-global-nav li:contains('Tools')" ).prepend('<i class="fa fa-wrench" aria-hidden="true"></i>');

    $(".js-match").each(function(){
        var match = $(this).data("match");
        match = match.replace(/\s+/g, "");
        if(location.href.indexOf(match) >= 0) {
            $(this).addClass("stay");
        }
    });
});

// オフキャンバスのブレイクポイントの設定
ACMS.Ready(function(){
    ACMS.Config.offcanvas.breakpoint = 1023
});
