var map;
var marker = [];
var infoWindow = [];
var markerData = [];
 
function initMap() {
     // 緯度経度のデータ作成

     // 現在位置情報を取得
    window.navigator.geolocation.getCurrentPosition(show_location);

    // 現在位置情報の取得に成功した時の処理
    function show_location(event) {
        // Google Map を表示する
        var myLatlng = new google.maps.LatLng(event.coords.latitude, event.coords.longitude); 
        var myOptions = {
            zoom: 15,
            center: myLatlng,
        }
        var map = new google.maps.Map(document.getElementById("map"), myOptions);

        var image = {
            url : 'http://kigiroku.com/themes/blog_ver2/tabi/images/mypin.png',
            scaledSize : new google.maps.Size(25, 40)
        }

        new google.maps.Marker({
            position: myLatlng, 
            map: map,
            icon: image
        });
    
        console.log(myLatlng);

    $.getJSON("http://kigiroku.com/themes/blog_ver2/tabi/ajax/map.json", function(json){
        for (var i = 0; i <= json.length-1; i++) {
            // alert("取得したデータ：" + json[i].name + json[i].spot);
            markerData.push(
                {
                    'name': json[i].name,
                    // 'spot': json[i].spot,
                    'comment': json[i].comment,
                    'lat': json[i].lat,
                    'lng': json[i].lng,
                    // 'zoom': json[i].zoom,
                }
            );
        };

    // map = new google.maps.Map(document.getElementById('map'), { // #mapに地図を埋め込む
    //     center: {lat: 35.179817, lng: 136.907055}, // 地図の中心
    //     zoom: 10 // 地図のズーム
    // });
 
        // マーカー毎の処理
        for (var i = 0; i < markerData.length; i++) {
            markerLatLng = new google.maps.LatLng(markerData[i]['lat'],markerData[i]['lng']); // 緯度経度のデータ作成
            marker[i] = new google.maps.Marker({ // マーカーの追加
                position: markerLatLng, // マーカーを立てる位置を指定
                map: map // マーカーを立てる地図を指定
            });
     
            infoWindow[i] = new google.maps.InfoWindow({ // 吹き出しの追加
                content: '<div style="width: 220px;"><p>' + markerData[i]['name'] + '</p>'
                         +'<p>'+ markerData[i]['comment'] + '</p></div>'
                         +'<a href="/blog/tabi/guide/"><input type="button" value="この人の詳細を見る"/></a>'
                // 吹き出しに表示する内容
            });
     
            markerEvent(i); // マーカーにクリックイベントを追加
        }
     
        marker[0].setOptions({// マーカーのオプション設定
            icon: {
                url: markerData[0]['icon']// マーカーの画像を変更
            }
        });

        });//jsonの閉じ

    }

}//initMapの閉じ


//マーカーにクリックイベントを追加
function markerEvent(i) {
    marker[i].addListener('click', function() { // マーカーをクリックしたとき
        infoWindow[i].open(map, marker[i]); // 吹き出しの表示
    });
}

//現在地の取得
function getGps(){
    var tmp = new Array();
    if( navigator.geolocation ){
        navigator.geolocation.getCurrentPosition(
            function( pos ){ //位置取得成功
                tmp['long'] = pos.coords.longitude; //経度
                tmp['lat'] = pos.coords.latitude; //緯度
                initialize( tmp );
            },
            function( error ){ //失敗
                switch( error.code ){
                    case 1: tmp['msg'] = "位置情報の利用が許可されていません"; break;
                    case 2: tmp['msg'] = "デバイスの位置が判定できません"; break;
                    case 3: tmp['msg'] = "タイムアウトしました"; break;
                }
                initialize( tmp );
            }
        );
    } else { //使用不可のブラウザ
        tmp['msg'] = 'このブラウザでは位置取得が出来ません。';
        initialize( tmp );
    }
}




   
