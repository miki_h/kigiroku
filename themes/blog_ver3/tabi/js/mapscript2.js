
var map;
 
function initMap() {
     // 緯度経度のデータ作成

     // 現在位置情報を取得
    window.navigator.geolocation.getCurrentPosition(show_location);

    // 現在位置情報の取得に成功した時の処理
    function show_location(event) {
        // Google Map を表示する
        var myLatlng = new google.maps.LatLng(event.coords.latitude, event.coords.longitude); 
        var myOptions = {
            zoom: 15,
            center: myLatlng,
        }
        var map = new google.maps.Map(document.getElementById("map"), myOptions);

        var image = {
            url : 'http://kigiroku.com/themes/blog_ver2/tabi/images/mypin.png',
            scaledSize : new google.maps.Size(25, 40)
        }

        new google.maps.Marker({
            position: myLatlng, 
            map: map,
            icon: image
        });

    var nagoya = {
        lat: 35.170915,
        lng: 136.881537
    };
 
    var map = new google.maps.Map(document.getElementById('map'), {
        center: myLatlng,
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    });
 
    var directionsDisplay = new google.maps.DirectionsRenderer({
        map: map, // 対象のGoogleMapsを指定
        suppressMarkers: true
    });
    var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: myLatlng, // スタート地点
        destination: nagoya, // ゴール地点
        travelMode: google.maps.TravelMode.WALKING // 移動手段：徒歩
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });

    }

}//initMapの閉じ


//現在地の取得
function getGps(){
    var tmp = new Array();
    if( navigator.geolocation ){
        navigator.geolocation.getCurrentPosition(
            function( pos ){ //位置取得成功
                tmp['long'] = pos.coords.longitude; //経度
                tmp['lat'] = pos.coords.latitude; //緯度
                initialize( tmp );
            },
            function( error ){ //失敗
                switch( error.code ){
                    case 1: tmp['msg'] = "位置情報の利用が許可されていません"; break;
                    case 2: tmp['msg'] = "デバイスの位置が判定できません"; break;
                    case 3: tmp['msg'] = "タイムアウトしました"; break;
                }
                initialize( tmp );
            }
        );
    } else { //使用不可のブラウザ
        tmp['msg'] = 'このブラウザでは位置取得が出来ません。';
        initialize( tmp );
    }
}




   


