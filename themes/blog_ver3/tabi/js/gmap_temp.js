(function($){
	var UNIV_LIST_MARK = "#marker_list-target";
	var UNIV_LIST_TPL = "#marker_list-tmpl";
	var MAP_CANVAS = 'map-canvas';
    var URL_CONTEXT = '/field/area/';
    var URL_TPL   = '/tabi/tpl/ajax/wcan.json';
    var UNIV_ITEM_MARK = ".infowindow_open";
    var SELECT_AREA_MARK = '#selectArea a';
    //URLコンテキスト取得
	var getUrl = function(){
		var d = new $.Deferred();
		var url = URL_TPL;
		d.resolve(url);
		return d.promise();
	}
	//URLより大学リストを取得
	var getUnivList = function(url){
		var d = new $.Deferred();
		$.getJSON(url,function(univs){
			d.resolve(univs);
		})
		return d.promise();
	}
	//大学リストよりテンプレートのビルド
	var buildUnivList = function(univs){
		var d = new $.Deferred();
		var mapOptions = {
            scrollwheel : false,
            mapTypeId   : google.maps.MapTypeId.ROADMAP,
            scrollwheel : false
        };
        var map = new google.maps.Map(document.getElementById(MAP_CANVAS), mapOptions);
        $(UNIV_LIST_MARK).html($(UNIV_LIST_TPL).tmpl(univs));
        d.resolve(univs,map);
        return d.promise();
	}
	//地図にマーカーを追加
	var addMarkers = function(univs,map){
		var markers = [];
		var i = 0;
		var deferres = [];
		for(var i = 0,n = univs.length; i < n; i++){
			(function(){
				var d = new $.Deferred();
				var marker;
				var univ = univs[i];
		        if ( univ.type === 'college' ) {
		            marker = new google.maps.Marker({
		                position: new google.maps.LatLng(univ.lat, univ.lng),
		                map: map,
		                icon: 'http://maps.google.co.jp/mapfiles/ms/icons/blue-dot.png',
		                draggable: false
		                // animation: google.maps.Animation.DROP // BOUNCE, DROP. Passing
		            });
		        } else {
		            marker = new google.maps.Marker({
		                position: new google.maps.LatLng(univ.lat, univ.lng),
		                map: map,
		                draggable: false
		                // animation: google.maps.Animation.DROP // BOUNCE, DROP. Passing
		            });
		        }
		        markers.push(marker);
		      	var info;
		        $.get(univ.infoView, function ( data ) {	
					var info = new google.maps.InfoWindow({
					    content: data
					});
					marker.info = info;
					d.resolve();
	            	google.maps.event.addListener(marker, 'click', function( ) {
	            		for (var i = 0, n = markers.length; i < n; i++) {
	            			markers[i].info.close();
	            		}
		                info.open(map, marker);
		                map.setZoom(14);
		                map.setCenter(marker.position);
		                map.panBy(0, -180);
		                google.maps.event.addListener(info, 'domready', function() {
		                    // document.getElementById('map-popup').parentNode.style.width='436px'; 
		                    // document.getElementById('map-popup').parentNode.style.overflow='';
		                    // document.getElementById('map-popup').parentNode.parentNode.style.overflow='';
		                    // document.getElementById('map-popup').parentNode.className='infoWindow';
		                });
	            	});
	            });
	            deferres.push(d);
		    })();
	    }
	    return $.when.apply(null,deferres).then(function(){
	    	var promise = new $.Deferred();
	    	promise.resolve(univs,map,markers);
	    	return promise;
	    });
	}
	//ちょうどいい位置にマップを合わせる
	var setBounds = function(univs,map,markers){
		var promise = new $.Deferred();
        var minLat=9999, minLng=9999, maxLat=-9999, maxLng=-9999;
        for (var i = 0; i < univs.length; i++ ) {
        	var univ = univs[i];
            if ( minLat > univ.lat ) minLat = univ.lat;
            if ( minLng > univ.lng ) minLng = univ.lng;
            if ( maxLat < univ.lat ) maxLat = univ.lat;
            if ( maxLng < univ.lng ) maxLng = univ.lng;
        }
        var minLatLng   = new google.maps.LatLng(minLat, minLng);
        var maxLatLng   = new google.maps.LatLng(maxLat, maxLng);
        var bounds      = new google.maps.LatLngBounds(minLatLng, maxLatLng);
        map.fitBounds(bounds);
        if ( map.getZoom() > 10 ) {
            // map.setZoom(5);
        }
		promise.resolve(univs,map,markers);
		return promise;
	}
	//イベント等のdispatch
	var dispatchEvent = function(univs,map,markers){
		$(UNIV_LIST_MARK).carouFredSel({
            auto: false,
            prev: '#prev',
            next: '#next'
        });
        $(UNIV_ITEM_MARK).click(function(e){
        	e.preventDefault();
	        var index   = 0;
	        var title   = $(this).attr('alt');
	        for (var i = 0, n = univs.length; i < n; i++) {
	            if ( univs[i].title === title ) {
	                index = i;
	                break;
	            }
	        }
	        google.maps.event.trigger(markers[index],"click");
        });
        $(SELECT_AREA_MARK).click(function(e){
        	e.preventDefault();
        	var area = $(this).attr('href').substring(1);
	        if ( area == 'all' ) {
	            url = URL_TPL;
	        } else {
	            url = URL_CONTEXT+area+URL_TPL;
	        }
			
			  var index = $(SELECT_AREA_MARK).index(this);
			  $('#selectArea li').removeClass('active');
			  $('#selectArea li').eq(index).addClass('active');

	        getUnivList(url)
	        .then(buildUnivList)
			.then(setBounds)
			.then(addMarkers)
			.then(dispatchEvent);
        });
        //ランダムにキャンバスを選択
        var rand = Math.floor( Math.random() * markers.length );
        google.maps.event.trigger(markers[rand],"click");
	}
	//実行
	$(function(){
		getUrl()
		.then(getUnivList)
		.then(buildUnivList)
		.then(setBounds)
		.then(addMarkers)
		.then(dispatchEvent);
	});
})(jQuery);