$(function() {



	$("nav .nav").hover(function () {
		var index = $("nav .nav").index(this);
		$('.nav img').eq(index).stop().animate({'marginLeft':'-0px'},'normal');
		},
		function () {
			$('.nav img').stop().animate({'marginLeft':'-390px'},'normal');
		}
	);



    var $setFilter = $('#filter'),
    $setList = $('#filterlist'),
    $setFilterAll = $('.allitem');
 
    var showFade = 500,
    showShut = 1000,
    hideFade = 1000,
    hideShut = 1000;
 
    var $setFilterBtn = $setFilter.children('a'),
    $setFilterList = $setList.children('ul').children('li'),
    $filterAllItem = $setFilterAll.attr('class');
 
    $setFilterBtn.click(function(){
        if(!($(this).hasClass('active'))){
            var filterClass = $(this).attr('class');
            $setFilterList.each(function(){
                if($(this).hasClass(filterClass)){
                    $(this).css({display:'block'});
                    $(this).find('*').stop().animate({opacity:'1'},showFade);
                    $(this).stop().animate({width:'300px'},showShut);
                } else {
                    $(this).find('*').stop().animate({opacity:'0'},hideFade);
                    $(this).stop().animate({width:'0'},hideShut,function(){
                        $(this).css({display:'none'});
                    });
                }
            });
            $setFilterBtn.removeClass('active');
            $(this).addClass('active');
        }
    });
 
    $setFilterAll.click(function(){
        $setFilterList.each(function(){
            $(this).css({display:'block'});
            $(this).find('*').stop().animate({opacity:'1'},showFade);
            $(this).stop().animate({width:'300px'},showShut);
        });
    });
    $setFilterAll.click();

    $(".filterImg").hover(function () {
        var Imgindex = $(".filterImg").index(this);
        $('.filterImg img').eq(Imgindex).stop().animate({opacity:'0.1'},'normal');
        },
        function () {
            $('.filterImg img').stop().animate({opacity:'1'},'normal');
        }
    );
    
});

$(function(){
 
    // サイトアクセス時にbodyを非表示にしてから、フェードインさせる
    $('.mainColumn').hide();
    $('.mainColumn').fadeIn(2000);
     
    // リンククリック時にフェードアウトしてから、画面遷移する
    $('a.fadelink').click(function(){
        // URLを取得する
        var url = $(this).attr('href');
 
        // URLが空ではない場合
        if (url != '') {
            // フェードアウトしてから、取得したURLにリンクする
            $('.mainColumn').fadeOut(1000);
            setTimeout(function(){
                location.href = url;
            }, 1000);
        }
        return false;
 
    });
});

