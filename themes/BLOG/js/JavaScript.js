

/*pagetop 上へ戻る*/

$(document).ready(function() {
    var pagetop = $('.arrow');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            pagetop.fadeIn();
        } else {
            pagetop.fadeOut();
        }
    });
    
    pagetop.click(function () {
        $('body, html').animate({ scrollTop: 0 }, 500);
        return false;
    });
});

/*ローディング画面*/

window.onload = function(){
    $(function() {
        $("#loading").fadeOut();
    });
}

$(function() {
    var h = $(window).height();
    $('#weblog').css('display','none');
    $('#loader-bg ,#loader').height(h).css('display','block');
});

$(window).load(function () {
    $('#loader-bg').delay(900).fadeOut(800);
    $('#loader').delay(600).fadeOut(300);
    $('#weblog').css('display', 'block');
});

//10秒たったら強制的にロード画面を非表示
$(function(){
    setTimeout('stopload()',10000);
});

function stopload(){
    $('#weblog').css('display','block');
    $('#loader-bg').delay(900).fadeOut(800);
    $('#loader').delay(600).fadeOut(300);
}

