var map;
var marker = [];
var data = [ 
    {
        name: '名古屋駅',
        lat: 35.170897,
        lng: 136.881558 
    }, {
        name: '大名古屋ビルヂング',
        lat: 35.172311,
        lng: 136.884568
    }, {
        name: '国際センター駅',
        lat: 35.172038, 
        lng: 136.887701
    }
];
 
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 35.170981, lng: 136.881556} ,
        zoom: 15
	});
 
    for (var i = 0; i < data.length; i++) {
        markerLatLng = {lat: data[i]['lat'], lng: data[i]['lng']}; 
        marker[i] = new google.maps.Marker({ 
            position: markerLatLng, 
            map: map 
        });
    }
 
}
 
