        var map;
        var marker = [];
        var data = [];
        var windows = [];
        var currentInfoWindow = null;
         
        function initMap() {
             // 緯度経度のデータ作成

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 35.170981, lng: 136.881556} ,
                zoom: 15
            });

            $.getJSON("http://kigiroku.com/gmap/3/map.json", function(json){
                for (var i = 0; i <= json.length-1; i++) {
                    data.push(
                        {
                            'name': json[i].name,
                            'lat': json[i].lat,
                            'lng': json[i].lng
                        }
                    );
                };
         
                for (var i = 0; i < data.length; i++) {
                    markerLatLng = {lat: data[i]['lat'], lng: data[i]['lng']}; 
                    marker[i] = new google.maps.Marker({ 
                        position: markerLatLng, 
                        map: map 
                    });

                    windows[i] = new google.maps.InfoWindow({ // 吹き出しの追加
                        content: '<div style="width: 220px;"><p>' + 
                                    data[i]['name'] + '</p>'+
                                    '<input type="button" value="この場所の詳細"/>'
                        // 吹き出しに表示する内容
                    });
                    markerEvent(i); // マーカーにクリックイベントを追加
                    
                }

            });//jsonの閉じ


        }//initMapの閉じ


        //マーカーにクリックイベントを追加
        function markerEvent(i) {
            marker[i].addListener('click', function() { // マーカーをクリックしたとき
                if (currentInfoWindow) {
                    currentInfoWindow.close();
                }
                windows[i].open(map, marker[i]); // 吹き出しの表示
                currentInfoWindow = windows[i];
            });
        }