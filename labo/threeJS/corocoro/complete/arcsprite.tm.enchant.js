/**
 * @author phi
 */

"use strict";

/**
 * tm ネームスペース
 */
enchant.tm = enchant.tm || {};

/**
 * @scope   ArcSprite.prototype
 */
enchant.tm.ArcSprite = enchant.Class.create(enchant.Sprite, {
    
    /**
     * 円弧スプライトクラス
     * @param {Number} radius   半径
     * @param {Object} color    色オブジェクト {r:255, g:0, b:0}
     */
    initialize: function(radius, color) {
        // 半径のデフォルト値
        radius = radius || 16;
        // 色を文字列にする関数
        var colorToStr = function(color, alpha) {
            return "rgba("+color.r+','+color.g+','+color.b+','+alpha+')';
        };
        // カラーのデフォルト値
        color = color || {r:255, g:0, b:0};
        // 直径
        var diameter = radius*2;
        // 親のコンストラクタを呼び出す
        enchant.Sprite.call(this, diameter, diameter);
        
        // サーフェスを生成
        var surface = new Surface(diameter, diameter);
        var c = surface.context;
        var grad = c.createRadialGradient(radius, radius, 0, radius, radius, radius);   // グラデーションを生成
        grad.addColorStop( 0.0, colorToStr(color, 0.0) );
        grad.addColorStop( 0.8, colorToStr(color, 1.0) );
        grad.addColorStop( 1.0, colorToStr(color, 0.0) );
        c.fillStyle = grad;
        c.beginPath();
        c.arc(radius, radius, radius, 0, Math.PI*2, true);
        c.fill();
        
        // スプライトにサーフィスをイメージとして登録
        this.image = surface;
    },
    
    /**
     * 中心位置
     * @type {Object}
     */
    center: {
        get: function() {
            return {
                "x" : this.x + this.width/2,
                "y" : this.y + this.height/2
            };
        }
    },
    
    /**
     * ランダムな方向を向いたベクトルをセットする
     */
    setRandomDirection: function() {
        // サンプル用に向きをセット
        var rad = Math.random()*360 * Math.PI/180;
        this.dx = Math.cos(rad);
        this.dy =-Math.sin(rad);
    },
    
    /**
     * 位置調整
     * @param {Number} x        はみ出さないようにしたい x 座標
     * @param {Number} y        はみ出さないようにしたい y 座標
     * @param {Number} width    はみ出さないようにしたい幅
     * @param {Number} height   はみ出さないようにしたい高さ
     */
    adjustPosition: function(x, y, width, height) {
        // スプライトの上下左右位置
        var left    = this.x;
        var right   = this.x + this.width;
        var top     = this.y;
        var bottom  = this.y + this.height;
        // はみ出た際の処理
        if (left   < x) { this.x = x; this.dx*=-1; }
        if (top    < y) { this.y = y; this.dy*=-1; }
        if (right  > width) { this.x = width -this.width;  this.dx*=-1; }
        if (bottom > height){ this.y = height-this.height; this.dy*=-1; }
    }
});













