/**
 * @author phi
 */

"use strict";


// enchant 初期化(global 領域に enchant と enchant.tm を追加)
enchant("tm");

/**
 * ロード
 */
window.onload = function()
{
    // ゲームクラスを生成
    var game = new Game(320, 320);
    game.score = 0;
    game.enemyList = [];
    
    // ゲーム開始時の処理
    game.onload = function(){
        // スコアラベルを表示
        var score_label = new Label();
        game.rootScene.addChild(score_label);
        score_label.moveTo(12, 20);
        score_label.color = "white";
        score_label.addEventListener(Event.ENTER_FRAME, function(){
            this.text = "スコア : " + game.score;
        });
        
        // プレイヤーを生成
        var player = new Player(20, {r:255, g:255, b:0});
        game.rootScene.addChild(player);
        player.moveTo(320/2-10, 320/2-10);
        player.dx = 0; player.dy = 0;
        player.addEventListener(Event.ENTER_FRAME, player.appear);
        
        // 加速度センサーでプレイヤーの向きベクトルの向き, 長さが変更する
        window.addEventListener('devicemotion', function(e){
            var gravity = e.accelerationIncludingGravity;
            player.dx += gravity.x*0.02;
            player.dy -= gravity.y*0.02;
        });
        
        // エネミーを生成
        for (var i=0; i<8; ++i) {
            var radius = 8;
            var color = {r: 255,g: 0,b: 0};
            // ArcSprite を生成
            var arc_sprite = new ArcSprite(radius, color);
            // 位置をセット
            arc_sprite.x = Math.random()*320;
            arc_sprite.y = Math.random()*320;
            // 向きベクトルをセット
            arc_sprite.setRandomDirection();
            // スピードを設定
            arc_sprite.speed = 0.5;
            // フレームイベントを登録
            arc_sprite.addEventListener(Event.ENTER_FRAME, function(){
                this.moveBy(this.dx*this.speed, this.dy*this.speed);
                this.adjustPosition(0, 0, game.width, game.height);
                
                if (game.frame/game.fps % 10 == 0) {
                    this.speed += 0.1;
                }
            });
            // ルートシーンに ArcSprite を追加
            game.rootScene.addChild(arc_sprite);
            game.enemyList.push(arc_sprite);
        }
        
        // ポイント
        var point = new ArcSprite(8, {r:255, g:255, b:0});
        game.rootScene.addChild(point);
        point.moveTo( Math.random()*(320-4), Math.random()*(320-4) );
        game.point = point;
    };
    
    // 背景表示
    game.rootScene.backgroundColor = 'rgb(0, 0, 0)';
    // ゲームスタート
    game.start();
}

/**
 * @scope   Player.prototype
 */
var Player = Class.create(ArcSprite, {
    
    /**
     * 円弧スプライトクラス
     * @param {Number} radius   半径
     * @param {Object} color    色オブジェクト {r:255, g:0, b:0}
     */
    initialize: function(radius, color) {
        // 親のコンストラクタを呼び出す
        ArcSprite.call(this, radius, color);
    },
    
    appear: function()
    {
        // ゲームインスタンスを取得
        var game = Game.instance;
        // 点滅
        this.opacity = (game.frame%8 == 0) ? 0.0 : 1.0;
        
        // 移動
        this.moveBy(this.dx, this.dy);
        this.adjustPosition(0, 0, game.width, game.height);
        
        // ４秒後にゲームに移行する(4秒間は無敵時間)
        if (game.frame/game.fps > 2) {
            // 現在のフレーム関数を解除
            this.removeEventListener(Event.ENTER_FRAME, arguments.callee);
            // 新たなフレーム関数を登録
            this.addEventListener(Event.ENTER_FRAME, this.update);
        }
    },
    
    update: function()
    {
        // ゲームインスタンスを取得
        var game = Game.instance;
        // 移動
        this.moveBy(this.dx, this.dy);
        this.adjustPosition(0, 0, game.width, game.height);
        // ポイントとの判定
        if (this.intersect(game.point)==true) {
            // スコアを加算
            game.score += 100;
            // また別の場所へ移動
            game.point.moveTo( Math.random()*(320-2), Math.random()*(320-2) );
        }
        // 敵との衝突判定
        var len=game.enemyList.length;
        for (var i=0; i<len; ++i) {
            if ( this.intersect(game.enemyList[i])==true ) {
                changeTweetScene();
            }
        }
    }
    
});

var changeTweetScene = function()
{
    // ゲームインスタンスを取得
    var game = Game.instance;
    // 入れ替えるシーンを生成
    var scene = new Scene();
    scene.backgroundColor = "rgba(0, 0, 0, 0.5)";
    game.replaceScene(scene);
    
    var tweet_label = new TweetLabel();
    tweet_label.moveToCenter(game.width, game.height);
    
    tweet_label.addEventListener(enchant.Event.TOUCH_START, function()
    {
        this.tweet("あなたのスコアは " + game.score + "point です. 遊んでくれてありがとう. \nこのゲームの作り方はこちら #javascript #game #html5",
            "http://tmlife.net/programming/javascript/enchant-js-iphone-accelerator-game.html?preview=true&preview_id=1591&preview_nonce=c0d2357bb9");
    });
    scene.addChild(tweet_label);
}





