/**
 * @author phi
 */

"use strict";


// enchant 初期化(global 領域に enchant と enchant.tm を追加)
enchant("tm");

/**
 * ロード
 */
window.onload = function()
{
    // ゲームクラスを生成
    var game = new Game(320, 320);
    game.enemyList = [];
    
    // ゲーム開始時の処理
    game.onload = function(){
        // プレイヤーを生成
        var player = new Player(20, {r:255, g:255, b:0});
        game.rootScene.addChild(player);
        player.moveTo(320/2-10, 320/2-10);
        player.dx = 0; player.dy = 0;
        player.addEventListener(Event.ENTER_FRAME, player.appear);
        
        // エネミーを生成
        for (var i=0; i<8; ++i) {
            var radius = 8;
            var color = {r: 255,g: 0,b: 0};
            // ArcSprite を生成
            var arc_sprite = new ArcSprite(radius, color);
            // 位置をセット
            arc_sprite.x = Math.random()*320;
            arc_sprite.y = Math.random()*320;
            // 向きベクトルをセット
            arc_sprite.setRandomDirection();
            // フレームイベントを登録
            arc_sprite.addEventListener(Event.ENTER_FRAME, function(){
                this.moveBy(this.dx*0.5, this.dy*0.5);
                this.adjustPosition(0, 0, game.width, game.height);
            });
            // ルートシーンに ArcSprite を追加
            game.rootScene.addChild(arc_sprite);
            game.enemyList.push(arc_sprite);
        }
    };
    
    // 背景表示
    game.rootScene.backgroundColor = 'rgb(0, 0, 0)';
    // ゲームスタート
    game.start();
}

/**
 * @scope   Player.prototype
 */
var Player = Class.create(ArcSprite, {
    
    /**
     * 円弧スプライトクラス
     * @param {Number} radius   半径
     * @param {Object} color    色オブジェクト {r:255, g:0, b:0}
     */
    initialize: function(radius, color) {
        // 親のコンストラクタを呼び出す
        ArcSprite.call(this, radius, color);
    },
    
    appear: function()
    {
        // ゲームインスタンスを取得
        var game = Game.instance;
        // 点滅
        this.opacity = (game.frame%8 == 0) ? 0.0 : 1.0;
        
        // 移動
        this.moveBy(this.dx, this.dy);
        this.adjustPosition(0, 0, game.width, game.height);
        
        // ４秒後にゲームに移行する(4秒間は無敵時間)
        if (game.frame/game.fps > 2) {
            // 現在のフレーム関数を解除
            this.removeEventListener(Event.ENTER_FRAME, arguments.callee);
            // 新たなフレーム関数を登録
            this.addEventListener(Event.ENTER_FRAME, this.update);
        }
    },
    
    update: function()
    {
        // ゲームインスタンスを取得
        var game = Game.instance;
        // 移動
        this.moveBy(this.dx, this.dy);
        this.adjustPosition(0, 0, game.width, game.height);
    }
    
});



