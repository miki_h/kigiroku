/**
 * @author phi
 */

"use strict";

/**
 * tm ネームスペース
 */
enchant.tm = enchant.tm || {};

/**
 * @scope   TweetLabel.prototype
 */
enchant.tm.TweetLabel = enchant.Class.create(enchant.Label, {
    
    /**
     * ツイートラベル
     * @param {String} text     テキスト
     */
    initialize: function(text) {
        // 親のコンストラクタを呼び出す
        text = text || "ツイートする";
        enchant.Label.call(this, text);
        // 幅をセット
        this.width = 12*text.length+12;
        // スタイルを設定
        var s = this._element.style;
        s.fontWeight = "bold";
        s.textAlign = "center";
        s.cursor = "pointer";
        s.color = "rgb(255, 255, 255)";
        s.borderRadius = "8px";
        s.display = "block";
        s.padding = "4px";
        s.backgroundColor = "rgb(180, 220, 255)";
    },
    
    /**
     * 中央に移動する
     */
    moveToCenter: function(width, height)
    {
        this.x = width/2  - this.width/2;
        this.y = height/2 - this.height/2;
    },
    
    /**
     * ツイートする
     * @param {String} message  メッセージ
     * @param {String} url      url
     */
    tweet: function(message, url)
    {
        var EUC = encodeURIComponent;
        var twitter_url = "http://twitter.com/?status=";
        twitter_url = "http://twitter.com/intent/tweet?"
        var text = "text="+EUC(message)+"&";
        var url_text="url="+EUC(url);
        window.open(twitter_url + text + url_text, "twitter", "width=640, height=480");
    }
    
});







