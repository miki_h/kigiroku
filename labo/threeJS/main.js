enchant();
window.onload = function() {
    var game = new Game(320, 400); // 表示領域の大きさを設定
    game.fps = 24;                 // ゲームの進行スピードを設定
    game.preload('http://jsrun.it/assets/b/v/r/y/bvryL.png',
        'http://jsrun.it/assets/o/7/W/G/o7WGn.png',
        'http://jsrun.it/assets/8/X/N/O/8XNOU.png',
        'http://jsrun.it/assets/b/c/R/R/bcRRp.png',
        'http://jsrun.it/assets/j/A/R/h/jARh9.png',
        'http://jsrun.it/assets/5/S/s/A/5SsAr.png',
        'http://jsrun.it/assets/y/R/S/7/yRS7S.png',
        'http://jsrun.it/assets/6/C/2/e/6C2eJ.png',
        'http://jsrun.it/assets/l/p/e/b/lpebg.png');

    game.onload = function() { // ゲームの準備が整ったらメインの処理を実行します
        /**
        * タイトルシーン
        *
        * タイトルシーンを作り、返す関数です。
        */
        var createStartScene = function() {
            var scene = new Scene();                                // 新しいシーンを作る
            scene.backgroundColor = '#FF8B00';                      // シーンの背景色を設定
            // スタート背景設定
            var startBg = new Sprite(320, 400);                   // スプライトを作る
            startBg.image = game.assets['http://jsrun.it/assets/l/p/e/b/lpebg.png'];     // スタート画像を設定
            startBg.x = 0;                                      // 横位置調整
            startBg.y = 0;                                     // 縦位置調整
            scene.addChild(startBg);                             // シーンに追加
            // スタート画像設定
            var startImage = new Sprite(236, 48);                   // スプライトを作る
            startImage.image = game.assets['http://jsrun.it/assets/j/A/R/h/jARh9.png'];     // スタート画像を設定
            startImage.x = 42;                                      // 横位置調整
            startImage.y = 310;                                     // 縦位置調整
            scene.addChild(startImage);                             // シーンに追加
            // タイトルラベル設定
/*            var title = new Label('超える！');                     // ラベルを作る
            title.textAlign = 'center';                             // 文字を中央寄せ
            title.color = '#ffffff';                                // 文字を白色に
        title.x = 0;                                            // 横位置調整
            title.y = 96;                                           // 縦位置調整
            title.font = '28px sans-serif';                         // 28pxのゴシック体にする
            scene.addChild(title);                                  // シーンに追加
/*
            // サブタイトルラベル設定
/*            var subTitle = new Label('- ススメ！エルくん２  -');  // ラベルを作る
            subTitle.textAlign = 'center';                          // 文字中央寄せ
    title.x = 0;                                            // 横位置調整
            subTitle.y = 196;                                       // 縦位置調整
            subTitle.font = '14px sans-serif';                      // 14pxのゴシック体にする
            scene.addChild(subTitle);                               // シーンに追加
*/            
            // スタート画像にタッチイベントを設定
            startImage.addEventListener(Event.TOUCH_START, function(e) {
                game.replaceScene(createGameScene());    // 現在表示しているシーンをゲームシーンに置き換える
            });
            // タイトルシーンを返します。
            return scene;
        };
        /**
        * ゲームシーン
        *
        * ゲームシーンを作り、返す関数です。
        */
        var createGameScene = function() {
            var scene = new Scene();                            // 新しいシーンを作る
            scene.backgroundColor = '#3F1F00';
            
        var GROUND_LINE = 250;
        var SCROLL_SPEED = 10;
        var elle = new Sprite(64, 64);
        var scroll = 0; // スクロール量を記録する変数
        elle.image = game.assets['http://jsrun.it/assets/b/c/R/R/bcRRp.png'];
        elle.x = 60;
        elle.y = GROUND_LINE - elle.height;

        // 毎フレームイベントをシーンに追加
        game.addEventListener(Event.ENTER_FRAME, function(){
            elle.frame ++;
            if (elle.frame > 2) {
                elle.frame = 0;
            }
        });
        // シーンにタッチイベントを追加
        scene.addEventListener(Event.TOUCH_START, function(e){
            // エルくんをジャンプさせる
            elle.tl.moveBy(0, -100, 12, enchant.Easing.CUBIC_EASEOUT)  // 12フレームかけて現在の位置から上に120px移動
            .moveBy(0, 100, 12, enchant.Easing.CUBIC_EASEIN); // 12フレームかけて現在の位置から下に120px移動

        });
        // スクロールする背景1の設定
            var bg1 = new Sprite(320, 400);            // スプライトを作る
            bg1.image = game.assets['http://jsrun.it/assets/b/v/r/y/bvryL.png']; // 画像を設定
            bg1.x = 0;                                 // 横位置調整
            bg1.y = 0;                                 // 縦位置調整
            scene.addChild(bg1);              // シーンに追加
            // スクロールする背景2の設定
            var bg2 = new Sprite(320, 400);            // スプライトを作る
            bg2.image = game.assets['http://jsrun.it/assets/o/7/W/G/o7WGn.png']; // 画像を設定
            bg2.x = 320;                               // 横位置調整 320px右に配置(bg1の右隣に隙間なく並べる)
            bg2.y = 0;                                 // 縦位置調整
            scene.addChild(bg2);              // シーンに追加

        // ハードルの設定
        var hurdle = new Sprite(40, 90);          // スプライトを作る
        hurdle.image = game.assets['http://jsrun.it/assets/8/X/N/O/8XNOU.png']; // 画像を設定
        hurdle.x = -hurdle.width;                  // 横位置調整 画面外に隠しておく
        hurdle.y = GROUND_LINE - hurdle.height+10;    // 縦位置調整 ハードルの下端を地面の高さと合わせる
        scene.addChild(hurdle);                    // シーンに追加
        // まきびしの設定
        var makibishi = new Sprite(30, 36);          // スプライトを作る
        makibishi.image = game.assets['http://jsrun.it/assets/y/R/S/7/yRS7S.png']; // 画像を設定
        makibishi.x = -makibishi.width;                // 横位置調整 画面外に隠しておく
        makibishi.y = GROUND_LINE - makibishi.height;  // 縦位置調整 まきびし下端を地面の高さと合わせる
        scene.addChild(makibishi);                   // シーンに追加
        // 鳥の設定
        var bird = new Sprite(68, 68);             // スプライトを作る
        bird.image = game.assets['http://jsrun.it/assets/6/C/2/e/6C2eJ.png']; // 画像を設定
        bird.x = -bird.width;                      // 鳥を左側の画面外に隠します
        bird.y = 90;                              // 鳥の飛ぶ高さを設定します
        scene.addChild(bird);

        scene.addChild(elle);

        // エルくんの当たり判定用スプライトの設定
        var elle_hit = new Sprite(1, 1);           // スプライトを作る（幅1, 高さ1）
        // elle_hit.image =                        // 画像は設定しない（透明）
        elle_hit.x = elle.x + elle.width / 2;      // 横位置調整 エルくんの左右中央に配置
        elle_hit.y = elle.y + elle.height / 2;     // 縦位置調整エルくんの上下中央に配置
        scene.addChild(elle_hit);         // シーンに追加


        var scoreLabel = new Label("");            // ラベルを作る
        scoreLabel.color = '#fff';                 // 白色に設定
        scene.addChild(scoreLabel);       // シーンに追加


        // エルくんがやられた関数
        var elleDead = function() {
            elle.frame = 3;                       // エルくんを涙目にする
            alert("無念・・・");              // ポップアップメッセージを出す
            game.pushScene(createGameoverScene(scroll)); // ゲームオーバーシーンをゲームシーンに重ねる(push)
        }

        // 毎フレームイベントをシーンに追加
        scene.addEventListener(Event.ENTER_FRAME, function(){
            // 背景をスクロールさせる
            bg1.x -= SCROLL_SPEED;                // 背景1をスクロール
            bg2.x -= SCROLL_SPEED;                // 背景2をスクロール
            if (bg1.x <= -320) {                  // 背景1が画面外に出たら
                bg1.x = 320;                  // 画面右端に移動
            }
            if (bg2.x <= -320) {                  // 背景2が画面外に出たら
                bg2.x = 320;                  // 画面右端に移動
            }
            scroll += SCROLL_SPEED;                       // 走った距離を記録
            scoreLabel.text = scroll.toString()+'m走破'; // スコア表示を更新

            if (scroll % 640 === 0) {              // 640m走るごとに
                hurdle.x = 320;                    // ハードルを右端に移動(出現)
            }

if (scroll % 560 === 0) { // 560m走るごとに
  makibishi.x = 320;             // まきびしを右端に出現させます
}
if (scroll % 3000 === 0) {// 3000m走るごとに
    bird.x = 320;                // 鳥を右端に出現させます
}



            // 当たり判定用スプライトをエルくんの上下中心に置く
            elle_hit.x = elle.x + elle.width/2;
            elle_hit.y = elle.y + elle.height/2;

            // 障害物のスクロールと、エルくんとの接触の設定
            if (hurdle.x > -hurdle.width) {       // ハードルが出現している（画面内にある）とき
                hurdle.x -= SCROLL_SPEED;         // ハードルをスクロール
                if (hurdle.intersect(elle_hit)) { // ハードルとエルくんがぶつかったとき
                    elleDead();                   // エルくんがやられた関数を実行
                }
            }


        if (makibishi.x > -makibishi.width) {     // まきびしが出現している（画面内にある）とき
            makibishi.x -= SCROLL_SPEED;        // まきびしをスクロール
            if (makibishi.intersect(elle_hit)) {// まきびしとエルくんがぶつかったとき
                elleDead();                   // エルくんがやられた関数を実行
            }
        }
        if (bird.x > -bird.width) {           // 鳥が出現している（画面内にある）とき
            bird.x -= SCROLL_SPEED * 1.2;     // 鳥を1.2倍速でスクロール

            bird.frame ++;
            if (bird.frame > 4 ) {
                bird.frame = 0;
            }


            //if (bird.frame > 0) {             // 鳥のフレーム番号を0, 1, 0, 1と切り替えて羽ばたかせる
                //bird.frame = 0;
            //} else {
                //bird.frame = 1;
            //}
            if (bird.intersect(elle_hit)) {   // 鳥とエルくんがぶつかったとき
                elleDead();                   // エルくんがやられた関数を実行
            }
        }        

        });






            // ゲームシーンを返す
            return scene;
        };
        /**
        * ゲームオーバーシーン
        *
        * ゲームオーバーシーンを作り、返す関数です。
        * createGameoverScore(※引数) ※引数にスコアを入れると画面にスコアが表示されます
        * ※は任意の名前でOKで、カンマ区切りで複数設定できます。
        * 例) var createGameoverScore = function (resultScore, test1, test2) {
        */
        var createGameoverScene = function(scroll) {
            var scene = new Scene();                                   // 新しいシーンを作る
            scene.scene.backgroundColor = '#000';              // シーンの背景色を設定
            // ゲームオーバー画像を設定
            var gameoverImage = new Sprite(189, 97);                   // スプライトを作る
            gameoverImage.image = game.assets['http://jsrun.it/assets/5/S/s/A/5SsAr.png'];  // 画像を設定
            gameoverImage.x = 66;                                      // 横位置調整
            gameoverImage.y = 170;                                     // 縦位置調整
            scene.scene.addChild(gameoverImage);                             // シーンに追加
            // スコア表示用ラベルの設定
            var scoreLabel = new Label(scroll.toString());                        // ラベルを作る
            scoreLabel.width = 320;                                    // 幅を設定
            scoreLabel.textAlign = 'center';                           // 文字を中央寄せ
            scoreLabel.color = '#ffffff';                              // 文字を白色に
            scoreLabel.x = 0;                                          // 横位置調整
            scoreLabel.y = 12;                                         // 縦位置調整
            scoreLabel.font = '96px sans-serif';                       // 28pxのゴシック体にする
            scene.scene.addChild(scoreLabel);                                // シーンに追加
            // スコア説明ラベル設定
            var scoreInfoLabel = new Label('m走り抜いた');            // ラベルを作る
            scoreInfoLabel.width = 320;                                // 幅を設定
            scoreInfoLabel.textAlign = 'center';                       // 文字を中央寄せ
            scoreInfoLabel.color = '#ffffff';                          // 文字を白色に
            scoreInfoLabel.x = 0;                                      // 横位置調整
            scoreInfoLabel.y = 110;                                    // 縦位置調整
            scoreInfoLabel.font = '32px sans-serif';                   // 28pxのゴシック体にする
            scene.scene.addChild(scoreInfoLabel);

            // リトライラベル(ボタン)設定
            var retryLabel = new Label('もう一度遊ぶ');                  // ラベルを作る
            retryLabel.color = '#fff';                                 // 文字を白色に
            retryLabel.textAlign = 'center';
        retryLabel.x = 0;                                          // 横位置調整
            retryLabel.y = 300;                                        // 縦位置調整
            retryLabel.font = '20px sans-serif';                       // 20pxのゴシック体にする
            scene.addChild(retryLabel);                                // シーンに追加
            // リトライラベルにタッチイベントを設定
            retryLabel.addEventListener(Event.TOUCH_START, function(e) {
                game.replaceScene(createStartScene());    // 現在表示しているシーンをタイトルシーンに置き換える
            });
            return scene;
        };
        game.replaceScene(createStartScene());  // ゲームの_rootSceneをスタートシーンに置き換える
    }
    game.start(); // ゲームをスタートさせます
};